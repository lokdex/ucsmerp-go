import { createContext, useState, useEffect } from "react";
import { api } from "../api";
import axios from "axios";
import useRefreshToken from "../hooks/useRefreshToken";

const AuthContext = createContext({});

export const AuthProvider = ({ children }) => {
   const [auth, setAuth] = useState({});
   const [init, setInit] = useState(true);
   const refresh = async () => {
      try {
         const { data } = await api.get("http://localhost:9090/refresh");
         setAuth((prev) => {
            return {
               ...prev,
               accessToken: data.AccTkn,
               codusu: data.CodUsu,
               roles: data.Roles,
            };
         });
      } catch (error) {
      } finally {
         setInit(false);
      }
   };

   useEffect(() => {
      refresh();
   }, []);

   return <AuthContext.Provider value={{ auth, setAuth, init }}>{children}</AuthContext.Provider>;
};

export default AuthContext;
