import { useState } from "react";
import useApi from "../hooks/useApi";
import { useForm } from "react-hook-form";

import {
   Flex,
   Box,
   Text,
   Radio,
   RadioGroup,
   Stack,
   Container,
   SimpleGrid,
   FormErrorMessage,
   FormLabel,
   FormControl,
   Input,
   Button,
   useToast,
} from "@chakra-ui/react";

const RecuperarMovimientos = () => {
   const toast = useToast();
   const api = useApi();
   const [tipMov, setTipMov] = useState("C");

   const {
      handleSubmit,
      register,
      formState: { errors, isSubmitting },
   } = useForm();

   async function registrarMovimientos(values) {
      try {
         let cOperaci = tipMov == "C" ? "/registrarCompras" : "/registrarVentas";
         let { data } = await api.post(cOperaci, values);
         toast({
            title: "OK",
            description: "Movimientos registrados correctamente",
            status: "success",
            duration: 1500,
            isClosable: true,
         });
      } catch (error) {
         toast({
            title: "Error",
            description: error.response.data || "Error en la solicitud",
            status: "error",
            duration: 1500,
            isClosable: true,
         });
      }
   }

   return (
      <Flex flexDirection="column" h="100vh">
         <Box maxW="container.xxl" borderRadius="md" bg="white" border="1px solid gray" m={8}>
            <Box bg="secondary">
               <Flex alignItems="center">
                  <Text size="md" fontSize="xl" fontWeight="900" p={3}>
                     Recuperar Compras y Ventas
                  </Text>
               </Flex>
            </Box>
            <form onSubmit={handleSubmit(registrarMovimientos)}>
               <Container maxW="container.lg" centerContent>
                  <RadioGroup onChange={setTipMov} value={tipMov} m={4}>
                     <Stack direction="row">
                        <Radio value="C">Compras</Radio>
                        <Radio value="V">Ventas</Radio>
                     </Stack>
                  </RadioGroup>
                  <SimpleGrid columns={{ sm: 1, md: 2 }} spacing={10} w="100%">
                     <FormControl isInvalid={!!errors.DFECINI}>
                        <FormLabel htmlFor="dFecIni">Desde:</FormLabel>
                        <Input
                           id="dFecIni"
                           type="date"
                           {...register("DFECINI", {
                              required: "Este campo es necesario",
                           })}
                        />
                        <FormErrorMessage>{errors.DFECINI && errors.DFECINI.message}</FormErrorMessage>
                     </FormControl>
                     <FormControl isInvalid={!!errors.DFECFIN}>
                        <FormLabel htmlFor="dFecFin">Hasta:</FormLabel>
                        <Input
                           id="dFecFin"
                           type="date"
                           {...register("DFECFIN", {
                              required: "Este campo es necesario",
                           })}
                        />
                        <FormErrorMessage>{errors.DFECFIN && errors.DFECFIN.message}</FormErrorMessage>
                     </FormControl>
                  </SimpleGrid>
                  <SimpleGrid columns={{ sm: 1, md: 2 }} spacing={[5, null, 10]} w="60%" justifyContent="center" p={4}>
                     <Button type="submit" isLoading={isSubmitting} colorScheme="green" variant="solid">
                        ACEPTAR
                     </Button>
                     <Button as="a" href="http://localhost:3000" colorScheme="red" variant="solid">
                        SALIR
                     </Button>
                  </SimpleGrid>
               </Container>
            </form>
         </Box>
      </Flex>
   );
};

export default RecuperarMovimientos;
