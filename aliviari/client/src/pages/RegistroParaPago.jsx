import { useState } from "react";
import useApi from "../hooks/useApi";
import { useForm } from "react-hook-form";
import { BsCheck2Square } from "react-icons/bs";

import {
   Flex,
   Box,
   Text,
   Radio,
   RadioGroup,
   Container,
   SimpleGrid,
   FormErrorMessage,
   Table,
   Thead,
   Icon,
   Tbody,
   Tr,
   Th,
   Td,
   TableContainer,
   FormLabel,
   FormControl,
   Input,
   InputRightElement,
   Checkbox,
   CheckboxGroup,
   Button,
   useToast,
   InputGroup,
} from "@chakra-ui/react";

const RegistroParaPago = () => {
   const toast = useToast();
   const api = useApi();
   const [documentos, setDocumentos] = useState([]);
   const [idComp, setIdComp] = useState();
   const [notCre, setNotCre] = useState();
   const [nroRUC, setNroRUC] = useState("");

   const {
      handleSubmit,
      register,
      formState: { errors, isSubmitting },
   } = useForm();
   const {
      handleSubmit: handleSubmit2,
      register: register2,
      formState: { errors: errors2, isSubmitting: isSubmitting2 },
   } = useForm();

   async function recuperarDocumentos(values) {
      try {
         let { data } = await api.get("/comprobantesNotas", { params: { CNRORUC: values.CNRORUC } });
         console.log(data);
         setNroRUC(values.CNRORUC);
         setDocumentos(data);
      } catch (error) {
         toast({
            title: "Error",
            description: error.response.data || "Error en la solicitud",
            status: "error",
            duration: 1500,
            isClosable: true,
         });
      }
   }

   async function registrarParaPago(values) {
      try {
         console.log(values);
         let { data } = api.post("/registrarPago", values);
         toast({
            title: "OK",
            description: "Documentos registrados para pago correctamente",
            status: "success",
            duration: 1500,
            isClosable: true,
         });
         recuperarDocumentos({ CNRORUC: nroRUC });
      } catch (error) {
         toast({
            title: "Error",
            description: error.response.data || "Error en la solicitud",
            status: "error",
            duration: 1500,
            isClosable: true,
         });
      }
   }

   return (
      <Flex flexDirection="column" h="90vh">
         <Box maxW="container.xxl" borderRadius="md" bg="white" border="1px solid gray" m={8} overflowY="scroll">
            <Box bg="secondary">
               <Flex alignItems="center">
                  <Text size="md" fontSize="xl" fontWeight="900" p={3}>
                     Registro de comprobantes para pago con notas de crédito
                  </Text>
               </Flex>
            </Box>
            <Container maxW="container.lg" centerContent>
               <SimpleGrid columns={1} w="100%" justifyContent="center" p={4}>
                  <form onSubmit={handleSubmit(recuperarDocumentos)}>
                     <FormControl isInvalid={!!errors.CNRORUC}>
                        <FormLabel htmlFor="cNroRuc">Número de RUC:</FormLabel>
                        <InputGroup>
                           <Input
                              id="cNroRuc"
                              {...register("CNRORUC", {
                                 required: "Este campo es necesario",
                                 minLength: { value: 11, message: "El RUC debe tener al menos 11 caracteres" },
                              })}
                           />
                           <InputRightElement width="7rem">
                              <Button colorScheme="blue" type="submit" isLoading={isSubmitting} w="100%">
                                 Buscar
                              </Button>
                           </InputRightElement>
                        </InputGroup>
                        <FormErrorMessage>{errors.CNRORUC && errors.CNRORUC.message}</FormErrorMessage>
                     </FormControl>
                  </form>
               </SimpleGrid>
               <TableContainer mt={4} w="100%">
                  <form onSubmit={handleSubmit2(registrarParaPago)}>
                     <FormControl isInvalid={!!errors2.CIDCOMP}>
                        <RadioGroup onChange={setIdComp} value={idComp}>
                           <Table variant="simple">
                              <Thead bgColor="brand">
                                 <Tr>
                                    <Th color="white" fontSize="1em" colSpan={2}>
                                       Comprobantes
                                    </Th>
                                 </Tr>
                                 <Tr>
                                    <Th color="white" fontSize="1.2em">
                                       #
                                    </Th>
                                    <Th color="white" fontSize="1em">
                                       Nro. Comprobante
                                    </Th>
                                    <Th color="white" fontSize="1em">
                                       Fecha
                                    </Th>
                                    <Th color="white" fontSize="1em">
                                       Monto (S/)
                                    </Th>
                                    <Th color="white">
                                       <Icon as={BsCheck2Square} w={5} h={5} />
                                    </Th>
                                 </Tr>
                              </Thead>
                              <Tbody>
                                 {documentos?.ACOMPRO?.length ? (
                                    documentos?.ACOMPRO?.map((item, idx) => (
                                       <Tr key={`compro-${idx}`}>
                                          <Td>{idx + 1}</Td>
                                          <Td>{item.CNROCOM}</Td>
                                          <Td>{item.DFECHA}</Td>
                                          <Td>{item.NMONTO}</Td>
                                          <Td>
                                             <Radio
                                                value={item.CIDCOMP}
                                                {...register2("CIDCOMP", {
                                                   required: "Debe seleccionar un comprobante",
                                                })}
                                             />
                                          </Td>
                                       </Tr>
                                    ))
                                 ) : (
                                    <Tr>
                                       <Td colSpan={5}>NO HAY COMPROBANTES PENDIENTES</Td>
                                    </Tr>
                                 )}
                              </Tbody>
                           </Table>
                        </RadioGroup>
                        <FormErrorMessage>{errors2.CIDCOMP && errors2.CIDCOMP.message}</FormErrorMessage>
                     </FormControl>
                     <FormControl isInvalid={!!errors2.ANOTCRE}>
                        <CheckboxGroup onChange={setNotCre} value={notCre}>
                           <Table variant="simple">
                              <Thead bgColor="brand">
                                 <Tr>
                                    <Th color="white" fontSize="1em" colSpan={2}>
                                       Notas de crédito
                                    </Th>
                                 </Tr>
                                 <Tr>
                                    <Th color="white" fontSize="1.2em">
                                       #
                                    </Th>
                                    <Th color="white" fontSize="1em">
                                       Nro. de Nota
                                    </Th>
                                    <Th color="white" fontSize="1em">
                                       Fecha
                                    </Th>
                                    <Th color="white" fontSize="1em">
                                       Referencia
                                    </Th>
                                    <Th color="white" fontSize="1em">
                                       Monto (S/)
                                    </Th>
                                    <Th color="white">
                                       <Icon as={BsCheck2Square} w={5} h={5} />
                                    </Th>
                                 </Tr>
                              </Thead>
                              <Tbody>
                                 {documentos?.ANOTCRE?.length ? (
                                    documentos?.ANOTCRE?.map((item, idx) => (
                                       <Tr key={`notcre-${idx}`}>
                                          <Td>{idx + 1}</Td>
                                          <Td>{item.CNOTCRE}</Td>
                                          <Td>{item.DFECHA}</Td>
                                          <Td>{item.CNROCOM}</Td>
                                          <Td>{item.NMONTO}</Td>
                                          <Td>
                                             <Checkbox
                                                value={item.CIDNOTA}
                                                {...register2("ANOTCRE", {
                                                   required: "Debe seleccionar al menos una nota",
                                                })}
                                             />
                                          </Td>
                                       </Tr>
                                    ))
                                 ) : (
                                    <Tr>
                                       <Td colSpan={5}>NO HAY NOTAS DE CRÉDITO PENDIENTES</Td>
                                    </Tr>
                                 )}
                              </Tbody>
                           </Table>
                        </CheckboxGroup>
                        <FormErrorMessage>{errors2.ANOTCRE && errors2.ANOTCRE.message}</FormErrorMessage>
                     </FormControl>
                     <SimpleGrid
                        columns={{ sm: 1, md: 2 }}
                        spacing={[5, 10]}
                        w="60%"
                        justifyContent="center"
                        p={4}
                        mx="auto"
                     >
                        <Button type="submit" isLoading={isSubmitting2} colorScheme="green" variant="solid">
                           ACEPTAR
                        </Button>
                        <Button as="a" href="http://localhost:3000" colorScheme="red" variant="solid">
                           SALIR
                        </Button>
                     </SimpleGrid>
                  </form>
               </TableContainer>
            </Container>
         </Box>
      </Flex>
   );
};

export default RegistroParaPago;
