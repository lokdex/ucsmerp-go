import { useState, useEffect } from "react";
import useApi from "../hooks/useApi";
import { useForm } from "react-hook-form";

import ModalCrear from "../components/ModalCrearNota";
import ModalDetalle from "../components/ModalDetalleNota";

import { BsCheck2Square } from "react-icons/bs";
import {
   Flex,
   Box,
   Text,
   Table,
   TableContainer,
   FormControl,
   FormErrorMessage,
   Thead,
   Tbody,
   Tr,
   Td,
   Th,
   Icon,
   Radio,
   RadioGroup,
   Container,
   SimpleGrid,
   Button,
   useToast,
   useDisclosure,
} from "@chakra-ui/react";

const NotasDeCredito = () => {
   const toast = useToast();
   const api = useApi();

   const { isOpen, onOpen, onClose } = useDisclosure();
   const { isOpen: isOpen2, onOpen: onOpen2, onClose: onClose2 } = useDisclosure();

   const [notas, setNotas] = useState([]);
   const [nota, setNota] = useState([]);
   const [radValue, setRadValue] = useState();

   const {
      handleSubmit,
      register,
      formState: { errors, isSubmitting },
   } = useForm();

   async function recuperarNotas() {
      try {
         let { data } = await api.get("/notaCredito");
         setNotas(data);
      } catch (error) {
         console.log(error);
         toast({
            title: "Error",
            description: error.response.data || "Error en la solicitud",
            status: "error",
            duration: 1500,
            isClosable: true,
         });
      }
   }

   async function getNota(values) {
      try {
         let { data } = await api.get("/notaCredito", {
            params: {
               CIDNOTA: radValue,
            },
         });
         console.log(data);
         setNota(data);
         onOpen2();
      } catch (error) {
         toast({
            title: "Error",
            description: error.response.data || "Error en la solicitud",
            status: "error",
            duration: 1500,
            isClosable: true,
         });
      }
   }

   useEffect(() => {
      recuperarNotas();
   }, []);

   return (
      <Flex flexDirection="column" h="100vh">
         <Box maxW="container.xxl" borderRadius="md" bg="white" border="1px solid gray" m={8}>
            <Box bg="secondary">
               <Flex alignItems="center">
                  <Text size="md" fontSize="xl" fontWeight="900" p={3}>
                     Registro de notas de crédito
                  </Text>
               </Flex>
            </Box>
            <form onSubmit={handleSubmit(getNota)}>
               <Container maxW="container.lg" centerContent>
                  <TableContainer mt={4} w="90%">
                     <FormControl isInvalid={!!errors.CIDNOTA}>
                        <RadioGroup onChange={setRadValue} value={radValue}>
                           <Table variant="simple">
                              <Thead bgColor="brand">
                                 <Tr>
                                    <Th color="white" fontSize="1.2em">
                                       #
                                    </Th>
                                    <Th color="white" fontSize="1em">
                                       Nota de crédito
                                    </Th>
                                    <Th color="white" fontSize="1em">
                                       Fecha
                                    </Th>
                                    <Th color="white" fontSize="1em">
                                       Monto (S/)
                                    </Th>
                                    <Th color="white">
                                       <Icon as={BsCheck2Square} w={5} h={5} />
                                    </Th>
                                 </Tr>
                              </Thead>
                              <Tbody>
                                 {notas?.length ? (
                                    notas?.map((item, idx) => (
                                       <Tr key={`row-${idx}`}>
                                          <Td>{idx + 1}</Td>
                                          <Td>{item.CNOTCRE}</Td>
                                          <Td>{item.DFECHA}</Td>
                                          <Td>{item.NMONTO}</Td>
                                          <Td>
                                             <Radio
                                                value={item.CIDNOTA}
                                                {...register("CIDNOTA", {
                                                   required: "Debe seleccionar una nota",
                                                })}
                                             />
                                          </Td>
                                       </Tr>
                                    ))
                                 ) : (
                                    <Tr>
                                       <Td colSpan={5}>NO HAY NOTAS DE CRÉDITO PENDIENTES</Td>
                                    </Tr>
                                 )}
                              </Tbody>
                           </Table>
                        </RadioGroup>
                        <FormErrorMessage>{errors.CIDNOTA && errors.CIDNOTA.message}</FormErrorMessage>
                     </FormControl>
                  </TableContainer>
                  <SimpleGrid columns={{ sm: 1, md: 3 }} spacing={[5, 5]} w="70%" justifyContent="center" p={4}>
                     <Button onClick={onOpen} colorScheme="green" variant="solid">
                        NUEVA NOTA
                     </Button>
                     <Button isLoading={isSubmitting} type="submit" colorScheme="blue" variant="solid">
                        VER DETALLE
                     </Button>
                     <Button as="a" href="http://localhost:3000" colorScheme="red" variant="solid">
                        SALIR
                     </Button>
                  </SimpleGrid>
               </Container>
            </form>
         </Box>
         <ModalCrear isOpen={isOpen} onClose={onClose} update={recuperarNotas} />
         <ModalDetalle isOpen={isOpen2} onClose={onClose2} datos={nota} update={recuperarNotas} />
      </Flex>
   );
};

export default NotasDeCredito;
