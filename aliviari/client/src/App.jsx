import { useState } from "react";
import { Routes, Route } from "react-router-dom";
import useAuth from "./hooks/useAuth";
import Layout from "./components/Layout";
import RequireAuth from "./components/RequireAuth";
import RecuperarMovimientos from "./pages/RecuperarMovimientos";
import NotasDeCredito from "./pages/NotasDeCredito";
import RegistroParaPago from "./pages/RegistroParaPago";

import "./App.css";

function App() {
   const { init } = useAuth();

   return init ? (
      <></>
   ) : (
      <Routes>
         <Route path="/" element={<Layout />}>
            {/* Public routes */}
            {/* <Route path="login" element={<Login />} /> */}

            {/* Private routes */}
            <Route element={<RequireAuth allowedRoles={["PER"]} />}>
               <Route path="/Alv1000" element={<RecuperarMovimientos />} />
               <Route path="/Alv1010" element={<NotasDeCredito />} />
               <Route path="/Alv1020" element={<RegistroParaPago />} />
            </Route>

            {/* 404 */}
            {/* <Route path="*" element={<NotFound />} /> */}
         </Route>
      </Routes>
   );
}

export default App;
