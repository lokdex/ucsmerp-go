import { api } from "../api";
import useAuth from "./useAuth";

const useRefreshToken = () => {
   const { setAuth } = useAuth();
   const refresh = async () => {
      const { data } = await api.get("http://localhost:9090/refresh");
      setAuth((prev) => {
         return { ...prev, accessToken: data.AccTkn, codusu: data.CodUsu };
      });
      return data;
   };
   return refresh;
};

export default useRefreshToken;
