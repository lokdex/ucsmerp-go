import { api } from "../api";
import { useEffect } from "react";
import useRefreshToken from "./useRefreshToken";
import { useNavigate, useLocation, Link, Navigate } from "react-router-dom";
import useAuth from "./useAuth";

const useApi = () => {
   const refresh = useRefreshToken();
   const { auth } = useAuth();
   const navigate = useNavigate();
   const location = useLocation();
   useEffect(() => {
      const requestIntercept = api.interceptors.request.use(
         (config) => {
            if (!config.headers["Authorization"]) {
               config.headers["Authorization"] = `Bearer ${auth.accessToken}`;
            }
            return config;
         },
         (error) => Promise.reject(error)
      );

      const responseIntercept = api.interceptors.response.use(
         (response) => response,
         async (error) => {
            const prevRequest = error?.config;
            if (error?.response?.status == 403 && !prevRequest?.sent) {
               prevRequest.sent = true;
               const newAccessToken = await refresh();
               prevRequest.headers["Authorization"] = `Bearer ${newAccessToken}`;
               return api(prevRequest);
            } else if (error?.response?.status == 400) {
               console.log(location);
               window.location.replace(`http://localhost:3000/login?from=${location.pathname}`);
               // navigate("http://localhost:3000/login", { state: { from: location }, replace: true });
            }
            return Promise.reject(error);
         }
      );

      return () => {
         api.interceptors.request.eject(requestIntercept);
         api.interceptors.response.eject(responseIntercept);
      };
   }, [auth, refresh]);

   return api;
};

export default useApi;
