import { useLocation, Navigate, Outlet } from "react-router-dom";
import useAuth from "../hooks/useAuth";

const RequireAuth = ({ allowedRoles }) => {
   const { auth } = useAuth();
   const location = useLocation();
   return auth?.roles?.find((role) => allowedRoles?.includes(role.CodRol)) ? (
      <Outlet />
   ) : auth?.accessToken ? (
      window.location.replace("http://localhost:3000/unauthorized")
   ) : (
      window.location.replace(`http://localhost:3000/login?from=${location.pathname}&sub=""`)
   );
};

export default RequireAuth;
