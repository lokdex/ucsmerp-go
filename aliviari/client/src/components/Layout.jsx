import useAuth from "../hooks/useAuth";
import { api } from "../api";
import { Outlet, Link } from "react-router-dom";
import { Box, Flex, Image, Button, Spacer, Menu, MenuButton, MenuList, MenuItem, MenuDivider } from "@chakra-ui/react";
import Imagotipo from "../images/ucsm-imagotipo-blanco.svg";

import { BsChevronCompactDown } from "react-icons/bs";

const Layout = () => {
   const { setAuth, auth } = useAuth();
   const handleLogout = async () => {
      try {
         let { data } = await api.post("http://localhost:9090/logout");
         setAuth({});
      } catch (error) {
         console.log(error);
      }
   };
   return (
      <Box height="100%" width="100%">
         <Box bg="brand" p="0.5rem" height="7vh">
            <Flex alignItems="center">
               <Image src={Imagotipo} h="3rem" />
               <Spacer />
               {auth.codusu && (
                  <>
                     <Menu>
                        <MenuButton
                           as={Button}
                           variant="ghost"
                           _hover={{ bgColor: "#06A95F" }}
                           _expanded={{ bgColor: "#06A95F" }}
                           color="white"
                           rightIcon={<BsChevronCompactDown />}
                        >
                           Menú
                        </MenuButton>
                        <MenuList zIndex={9999}>
                           <MenuItem as={Link} to="Alv1000">
                              Recuperar Movimientos
                           </MenuItem>
                           <MenuItem as={Link} to="Alv1010">
                              Notas de crédito
                           </MenuItem>
                           <MenuItem as={Link} to="Alv1020">
                              Registro para pago
                           </MenuItem>
                           <MenuDivider />
                           <MenuItem as="a" href="http://localhost:3000">
                              Menú principal
                           </MenuItem>
                        </MenuList>
                     </Menu>
                     <Button variant="ghost" _hover={{ bgColor: "#06A95F" }} color="white" onClick={handleLogout}>
                        Cerrar Sesión
                     </Button>
                  </>
               )}
            </Flex>
         </Box>
         <Outlet />
         <Box textAlign="center" height="3vh">
            © 2021 Universidad Católica de Santa María. Todos los derechos reservados. UCSM
         </Box>
      </Box>
   );
};

export default Layout;
