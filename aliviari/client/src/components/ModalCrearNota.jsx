import { useState } from "react";
import useApi from "../hooks/useApi";
import { useForm } from "react-hook-form";

import {
   Select,
   FormErrorMessage,
   FormLabel,
   FormControl,
   Input,
   InputRightElement,
   NumberInput,
   NumberInputField,
   Button,
   useToast,
   Modal,
   ModalOverlay,
   ModalContent,
   ModalHeader,
   ModalFooter,
   ModalBody,
   ModalCloseButton,
   Textarea,
   InputGroup,
} from "@chakra-ui/react";

const ModalCrear = ({ isOpen, onClose, update }) => {
   const toast = useToast();
   const api = useApi();
   const [comprobantes, setComprobantes] = useState([]);
   const [nroRuc, setNroRuc] = useState("");
   const {
      handleSubmit,
      register,
      formState: { errors, isSubmitting },
   } = useForm();
   const {
      handleSubmit: handleSubmit2,
      register: register2,
      formState: { errors: errors2, isSubmitting: isSubmitting2 },
   } = useForm();

   async function buscarRUC(values) {
      try {
         let { data } = await api.get(`/buscarRUC?CNRORUC=${values.CNRORUC}`);
         if (data?.length == 0) {
            toast({
               title: "Info",
               description: "No hay comprobantes pendientes para el RUC consultado",
               status: "info",
               duration: 1500,
               isClosable: true,
            });
         } else {
            setComprobantes(data);
            setNroRuc(values.CNRORUC);
         }
      } catch (error) {
         toast({
            title: "Error",
            description: error.response.data || "Error en la solicitud",
            status: "error",
            duration: 1500,
            isClosable: true,
         });
      }
   }

   async function crearNota(values) {
      try {
         let response = await api.post("/notaCredito", { ...values, CNRORUC: nroRuc });
         update();
         onClose();
         toast({
            title: "OK",
            description: "Nota de crédito registrada correctamente",
            status: "success",
            duration: 1500,
            isClosable: true,
         });
      } catch (error) {
         toast({
            title: "Error",
            description: error.response.data || "Error en la solicitud",
            status: "error",
            duration: 1500,
            isClosable: true,
         });
      }
   }

   return (
      <Modal isOpen={isOpen} onClose={onClose} size="3xl">
         <ModalOverlay />
         <ModalContent>
            <ModalHeader>Registro de nueva nota de crédito</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
               <form onSubmit={handleSubmit(buscarRUC)}>
                  <FormControl isInvalid={!!errors.CNRORUC}>
                     <FormLabel htmlFor="cNroRuc">Nro. de RUC del proveedor</FormLabel>
                     <InputGroup>
                        <Input
                           id="cNroRuc"
                           {...register("CNRORUC", {
                              required: "Este campo es necesario",
                              minLength: { value: 11, message: "El RUC debe tener al menos 11 caracteres" },
                           })}
                        />
                        <InputRightElement width="7rem">
                           <Button colorScheme="blue" type="submit" isLoading={isSubmitting} w="100%">
                              Buscar
                           </Button>
                        </InputRightElement>
                     </InputGroup>
                     <FormErrorMessage>{errors.CNRORUC && errors.CNRORUC.message}</FormErrorMessage>
                  </FormControl>
               </form>
               <form onSubmit={handleSubmit2(crearNota)}>
                  <FormControl isInvalid={!!errors2.CIDCOMP}>
                     <FormLabel htmlFor="cIdComp">Seleccione compra</FormLabel>
                     <Select
                        id="cIdComp"
                        placeholder="Seleccione comprobante"
                        {...register2("CIDCOMP", {
                           required: "Seleccione una opcion",
                        })}
                     >
                        {comprobantes.length ? (
                           comprobantes.map((item, idx) => (
                              <option key={`opt-${idx}`} value={item.CIDCOMP}>
                                 {item.DFECHA} - {item.CNROCOM} - S/ {item.NMONTO}
                              </option>
                           ))
                        ) : (
                           <option></option>
                        )}
                     </Select>
                     <FormErrorMessage>{errors2.CIDCOMP && errors2.CIDCOMP.message}</FormErrorMessage>
                  </FormControl>
                  <FormControl isInvalid={!!errors2.CNOTCRE}>
                     <FormLabel htmlFor="cNotCre">Nro. Nota de crédito</FormLabel>
                     <Input
                        id="cNotCre"
                        {...register2("CNOTCRE", {
                           required: "Este campo es necesario",
                           pattern: {
                              value: /([A-Z0-9]{4})-([0-9]{8})/g,
                              message: "La serie debe ser de 4 caracteres y el correlativo de 8",
                           },
                        })}
                     />
                     <FormErrorMessage>{errors2.CNOTCRE && errors2.CNOTCRE.message}</FormErrorMessage>
                  </FormControl>
                  <FormControl isInvalid={!!errors2.NMONTO}>
                     <FormLabel htmlFor="nMonto">Monto</FormLabel>
                     <NumberInput precision={2}>
                        <NumberInputField
                           id="nMonto"
                           {...register2("NMONTO", {
                              required: "Este campo es necesario",
                              valueAsNumber: true,
                           })}
                        />
                     </NumberInput>
                     <FormErrorMessage>{errors2.NMONTO && errors2.NMONTO.message}</FormErrorMessage>
                  </FormControl>
                  <FormControl isInvalid={!!errors2.DFECHA}>
                     <FormLabel htmlFor="dFecha">Fecha</FormLabel>
                     <Input
                        id="dFecha"
                        type="date"
                        {...register2("DFECHA", {
                           required: "Este campo es necesario",
                        })}
                     />
                     <FormErrorMessage>{errors2.DFECHA && errors2.DFECHA.message}</FormErrorMessage>
                  </FormControl>
                  <FormControl isInvalid={!!errors2.MDATOS}>
                     <FormLabel htmlFor="mDatos">Datos</FormLabel>
                     <Textarea
                        id="mDatos"
                        type="date"
                        {...register2("MDATOS", {
                           required: "Este campo es necesario",
                        })}
                     />
                     <FormErrorMessage>{errors2.MDATOS && errors2.MDATOS.message}</FormErrorMessage>
                  </FormControl>
                  <ModalFooter>
                     <Button colorScheme="red" mr={3} onClick={onClose}>
                        CERRAR
                     </Button>
                     <Button colorScheme="green" type="submit" isLoading={isSubmitting2}>
                        GUARDAR
                     </Button>
                  </ModalFooter>
               </form>
            </ModalBody>
         </ModalContent>
      </Modal>
   );
};

export default ModalCrear;
