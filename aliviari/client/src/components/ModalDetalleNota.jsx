import { useState } from "react";
import useApi from "../hooks/useApi";
import { useForm } from "react-hook-form";

import {
   Select,
   FormErrorMessage,
   FormLabel,
   FormControl,
   Input,
   InputRightElement,
   NumberInput,
   NumberInputField,
   Button,
   useToast,
   Modal,
   ModalOverlay,
   ModalContent,
   ModalHeader,
   ModalFooter,
   ModalBody,
   ModalCloseButton,
   Textarea,
   InputGroup,
} from "@chakra-ui/react";

const ModalDetalle = ({ isOpen, onClose, datos, update }) => {
   const toast = useToast();
   const api = useApi();

   const {
      handleSubmit,
      formState: { isSubmitting },
   } = useForm();

   async function anularNota() {
      try {
         let response = await api.delete("/notaCredito", {
            params: {
               CIDNOTA: datos.CIDNOTA,
            },
         });
         update();
         onClose();
         toast({
            title: "OK",
            description: "Nota de crédito anulada",
            status: "success",
            duration: 1500,
            isClosable: true,
         });
      } catch (error) {
         console.log(error);
         toast({
            title: "Error",
            description: error.response.data || "Error en la solicitud",
            status: "error",
            duration: 1500,
            isClosable: true,
         });
      }
   }

   return (
      <Modal isOpen={isOpen} onClose={onClose} size="3xl">
         <ModalOverlay />
         <ModalContent>
            <ModalHeader>Detalle de nota de crédito</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
               <form onSubmit={handleSubmit(anularNota)}>
                  <FormControl>
                     <FormLabel htmlFor="cNroRuc">Nro. de RUC del proveedor</FormLabel>
                     <InputGroup>
                        <Input id="cNroRuc" value={datos.CNRORUC} readOnly />
                     </InputGroup>
                  </FormControl>
                  <FormControl>
                     <FormLabel htmlFor="cIdComp">Referencia</FormLabel>
                     <Input id="cNroRuc" value={datos.CIDCOMP} readOnly />
                  </FormControl>
                  <FormControl>
                     <FormLabel htmlFor="cNotCre">Nro. Nota de crédito</FormLabel>
                     <Input id="cNroRuc" value={datos.CNOTCRE} readOnly />
                  </FormControl>
                  <FormControl>
                     <FormLabel htmlFor="nMonto">Monto</FormLabel>
                     <Input id="cNroRuc" value={datos.NMONTO} readOnly />
                  </FormControl>
                  <FormControl>
                     <FormLabel htmlFor="dFecha">Fecha</FormLabel>
                     <Input id="cNroRuc" value={datos.DFECHA} readOnly />
                  </FormControl>
                  <FormControl>
                     <FormLabel htmlFor="mDatos">Datos</FormLabel>
                     <Textarea id="mDatos" value={datos.MDATOS} readOnly />
                  </FormControl>
                  <ModalFooter>
                     <Button colorScheme="red" mr={3} onClick={onClose}>
                        CERRAR
                     </Button>
                     <Button colorScheme="yellow" type="submit" isLoading={isSubmitting}>
                        ANULAR
                     </Button>
                  </ModalFooter>
               </form>
            </ModalBody>
         </ModalContent>
      </Modal>
   );
};

export default ModalDetalle;
