package handlers

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"
)

func GetComprobantes(c *gin.Context) {
	type Comprobantes struct {
		CIDCOMP string
		DFECHA  string
		CNROCOM string
		NMONTO  float64
	}
	lcNroRuc := c.DefaultQuery("CNRORUC", "")
	if lcNroRuc == "" {
		c.String(http.StatusBadRequest, "NÚMERO DE RUC NO DEFINIDO")
		return
	}
	comprobantes := []Comprobantes{}
	lcSql := `SELECT cIdComp, to_char(dFecha, 'YYYY-MM-DD') as dFecha, cNroCom, nMonto FROM E02MCOM
				WHERE cIdPago = '000' AND cNroRuc = $1
				ORDER BY dFecha DESC`
	rows, err := ERPPool.Query(context.Background(), lcSql, lcNroRuc)
	if err != nil {
		c.String(http.StatusInternalServerError, "ERROR RECUPERANDO COMPROBANTES")
		return
	}
	for rows.Next() {
		comprobante := Comprobantes{}
		if err := rows.Scan(&comprobante.CIDCOMP, &comprobante.DFECHA, &comprobante.CNROCOM, &comprobante.NMONTO); err != nil {
			c.String(http.StatusInternalServerError, "ERROR LEYENDO COMPROBANTES")
			return
		}
		comprobantes = append(comprobantes, comprobante)
	}
	c.JSON(http.StatusOK, comprobantes)
}

func GetComprobantesNotas(c *gin.Context) {
	type Comprobante struct {
		CIDCOMP string
		DFECHA  string
		CNROCOM string
		CNRORUC string
		CTIPCOM string
		CDESCRI string
		CESTADO string
		CMONEDA string
		NMONTO  float64
	}
	type Nota struct {
		CIDNOTA string
		CNOTCRE string
		DFECHA  string
		NMONTO  float64
		MDATOS  string
		CNROCOM string
	}
	lcNroRuc := c.DefaultQuery("CNRORUC", "")
	if lcNroRuc == "" {
		c.String(http.StatusBadRequest, "NÚMERO DE RUC NO DEFINIDO")
		return
	}
	comprobantes := []Comprobante{}
	notas := []Nota{}
	lcSql := `SELECT cIdComp, to_char(dFecha, 'YYYY-MM-DD') as dFecha, cNroCom, cNroRuc, cTipCom, cDescri, cEstado, cMoneda, nMonto
				FROM E02MCOM WHERE cNroRuc = $1 AND cEstado = 'A' AND cIdPago = '000'`
	rows, err := ERPPool.Query(context.Background(), lcSql, lcNroRuc)
	if err != nil {
		c.String(http.StatusInternalServerError, "ERROR RECUPERANDO COMPROBANTES")
		return
	}
	for rows.Next() {
		comp := Comprobante{}
		if err := rows.Scan(&comp.CIDCOMP, &comp.DFECHA, &comp.CNROCOM, &comp.CNRORUC, &comp.CTIPCOM,
			&comp.CDESCRI, &comp.CESTADO, &comp.CMONEDA, &comp.NMONTO); err != nil {
			c.String(http.StatusInternalServerError, "ERROR LEYENDO COMPROBANTES")
			return
		}
		comprobantes = append(comprobantes, comp)
	}
	lcSql = `SELECT a.cIdNota, a.cNotCre, to_char(a.dFecha, 'YYYY-MM-DD') as dFecha, a.nMonto, a.mDatos, b.cNroCom
				FROM E02MNCR a
				INNER JOIN E02MCOM b ON a.cIdComp = b.cIdComp
				WHERE a.cNroRuc = $1 AND a.cEstado = 'P' AND a.cIdPago = '000'`
	rows, err = ERPPool.Query(context.Background(), lcSql, lcNroRuc)
	if err != nil {
		c.String(http.StatusInternalServerError, "ERROR RECUPERANDO NOTAS DE CREDITO")
		return
	}
	for rows.Next() {
		nota := Nota{}
		if err := rows.Scan(&nota.CIDNOTA, &nota.CNOTCRE, &nota.DFECHA, &nota.NMONTO, &nota.MDATOS, &nota.CNROCOM); err != nil {
			c.String(http.StatusInternalServerError, "ERROR LEYENDO NOTAS DE CREDITO")
			return
		}
		notas = append(notas, nota)
	}
	type Res struct {
		ACOMPRO []Comprobante
		ANOTCRE []Nota
	}
	res := Res{
		ACOMPRO: comprobantes,
		ANOTCRE: notas,
	}
	c.JSON(http.StatusOK, res)
}

type Request struct {
	CIDPAGO string
	CIDCOMP string
	ANOTCRE []string
}

func RegistrarPago(c *gin.Context) {
	lcSql := `SELECT max(cIdPago) FROM E02MPAG`
	req := Request{}
	err := json.NewDecoder(c.Request.Body).Decode(&req)
	if err != nil {
		c.String(http.StatusBadRequest, "ERROR AL LEER REQUEST")
		return
	}
	err = ERPPool.QueryRow(context.Background(), lcSql).Scan(&req.CIDPAGO)
	if err != nil {
		c.String(http.StatusInternalServerError, "ERROR RECUPERANDO ÚLTIMO ID")
		return
	}
	req.CIDPAGO = Correlativo(req.CIDPAGO)
	lcSql = `INSERT INTO E02MPAG (cIdPago, cEstado, cUsuAli, cUsuCod) VALUES ($1, 'A', 'Q103', 'Q103')`
	_, err = ERPPool.Exec(context.Background(), lcSql, req.CIDPAGO)
	if err != nil {
		c.String(http.StatusInternalServerError, "ERROR CREANDO REGISTRO PARA PAGO")
		return
	}
	lcSql = `UPDATE E02MCOM SET (cIdPago, cEstado) = ($1, 'R') WHERE cIdComp = $2`
	_, err = ERPPool.Exec(context.Background(), lcSql, req.CIDPAGO, req.CIDCOMP)
	if err != nil {
		c.String(http.StatusInternalServerError, "ERROR REGISTRANDO COMPRA PARA PAGO")
		return
	}
	for _, laTmp := range req.ANOTCRE {
		lcSql = `UPDATE E02MNCR SET (cIdPago, cEstado) = ($1, 'R') WHERE cIdNota = $2`
		_, err = ERPPool.Exec(context.Background(), lcSql, req.CIDPAGO, laTmp)
		if err != nil {
			c.String(http.StatusInternalServerError, "ERROR REGISTRANDO NOTA DE CREDITO PARA PAGO")
			return
		}
	}
	c.AbortWithStatus(http.StatusOK)
}
