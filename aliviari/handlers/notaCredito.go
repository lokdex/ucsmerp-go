package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type NotaCredito struct {
	CIDNOTA string
	CNOTCRE string
	CNRORUC string
	CRAZSOC string
	CIDCOMP string
	DFECHA  string
	CESTADO string
	NMONTO  float64
	MDATOS  string
}

func GetNotaCredito(c *gin.Context) {
	lcIdNota := c.DefaultQuery("CIDNOTA", "")
	if lcIdNota == "" {
		lcSql := `SELECT cIdNota, cNotCre, cNroRuc, to_char(dFecha, 'YYYY-MM-DD') as dFecha, cEstado, nMonto
				FROM E02MNCR WHERE cEstado = 'P'`
		rows, err := ERPPool.Query(context.Background(), lcSql)
		if err != nil {
			c.String(http.StatusInternalServerError, "ERROR RECUPERANDO NOTAS DE CREDITO")
			return
		}
		notas := []NotaCredito{}
		for rows.Next() {
			nota := NotaCredito{}
			if err := rows.Scan(&nota.CIDNOTA, &nota.CNOTCRE, &nota.CNRORUC, &nota.DFECHA, &nota.CESTADO, &nota.NMONTO); err != nil {
				c.String(http.StatusInternalServerError, "ERROR LEYENDO NOTAS DE CREDITO")
				return
			}

			notas = append(notas, nota)
		}
		c.JSON(http.StatusOK, notas)
	} else {
		nota := NotaCredito{}
		lcSql := `SELECT a.cNotCre, a.cNroRuc, c.cRazSoc, b.cNroCom, to_char(a.dFecha, 'YYYY-MM-DD') as dFecha, a.cEstado, a.nMonto, a.mDatos
					FROM E02MNCR a
					INNER JOIN E02MCOM b ON a.cIdComp = b.cIdComp
					INNER JOIN S01MPRV c ON a.cNroRuc = c.cNroRuc
					WHERE cIdNota = $1`
		err := ERPPool.QueryRow(context.Background(), lcSql, lcIdNota).
			Scan(&nota.CNOTCRE, &nota.CNRORUC, &nota.CRAZSOC, &nota.CIDCOMP, &nota.DFECHA, &nota.CESTADO, &nota.NMONTO, &nota.MDATOS)
		if err != nil {
			fmt.Println(err)
			c.String(http.StatusInternalServerError, "ERROR RECUPERANDO NOTA DE CREDITO")
			return
		}
		nota.CIDNOTA = lcIdNota
		c.JSON(http.StatusOK, nota)
	}
}

func CrearNotaCredito(c *gin.Context) {
	nota := &NotaCredito{}
	err := json.NewDecoder(c.Request.Body).Decode(nota)
	if err != nil {
		c.String(http.StatusBadRequest, "ERROR EN FORMATO DE SOLICITUD")
		return
	}
	lcSql := `SELECT MAX(cIdNota) FROM E02MNCR`
	err = ERPPool.QueryRow(context.Background(), lcSql).Scan(&nota.CIDNOTA)
	if err != nil {
		nota.CIDNOTA = "00001"
	}
	nota.CIDNOTA = Correlativo(nota.CIDNOTA)
	lcSql = `INSERT INTO E02MNCR (cIdNota, cNotCre, cNroRuc, cIdComp, dFecha, cEstado, nMonto, mDatos, cUsuCod)
					VALUES ($1, $2, $3, $4, NOW(), 'P', $5, $6, 'Q103')`
	_, err = ERPPool.Exec(context.Background(), lcSql, nota.CIDNOTA, nota.CNOTCRE, nota.CNRORUC, nota.CIDCOMP, nota.NMONTO, nota.MDATOS)
	if err != nil {
		c.String(http.StatusInternalServerError, "ERROR AL REGISTRAR NOTA DE CREDITO")
		return
	}
	c.AbortWithStatus(http.StatusOK)
}

func AnularNotaCredito(c *gin.Context) {
	lcIdNota := c.DefaultQuery("CIDNOTA", "")
	if lcIdNota == "" {
		c.String(http.StatusBadRequest, "ERROR EN FORMATO DE SOLICITUD")
		return
	}
	lcSql := `UPDATE E02MNCR SET cEstado = 'X' WHERE cIdNota = $1`
	_, err := ERPPool.Exec(context.Background(), lcSql, lcIdNota)
	if err != nil {
		c.String(http.StatusInternalServerError, "ERROR AL ANULAR NOTA DE CREDITO")
		return
	}
	c.AbortWithStatus(http.StatusOK)
}
