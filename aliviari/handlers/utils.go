package handlers

import (
	"context"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Claims struct {
	NroDni string `json:"CNRODNI"`
	jwt.StandardClaims
}

var ERPPool *pgxpool.Pool

var jwtKey = []byte("INGLORIUS")

func InitDB() (*pgxpool.Pool, error) {
	return pgxpool.Connect(context.Background(), os.Getenv("DB_ERP"))
}

func ValidateAccessToken(c *gin.Context) int {
	auth := c.Request.Header.Get("Authorization")
	if auth == "" {
		return http.StatusUnauthorized
	}
	claims := &Claims{}
	token, err := jwt.ParseWithClaims(auth, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			return http.StatusUnauthorized
		}
		return http.StatusForbidden
	}
	if !token.Valid {
		return http.StatusUnauthorized
	}
	return 0
}

func ValidateRefreshToken(c *gin.Context) (int, *Claims) {
	cookie, err := c.Cookie("UCSM")
	if err != nil {
		if err == http.ErrNoCookie {
			return http.StatusUnauthorized, nil
		}
		return http.StatusBadRequest, nil
	}
	claims := &Claims{}
	token, err := jwt.ParseWithClaims(cookie, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			return http.StatusUnauthorized, nil
		}
		return http.StatusBadRequest, nil
	}
	if !token.Valid {
		return http.StatusUnauthorized, nil
	}
	return 0, claims
}

func ValidateToken(c *gin.Context) {
	auth := c.Request.Header.Get("Authorization")
	auth = strings.Split(auth, " ")[1]
	if auth == "" {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}
	claims := &Claims{}
	token, err := jwt.ParseWithClaims(auth, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		} else {
			c.AbortWithStatus(http.StatusForbidden)
			return
		}
	}
	if !token.Valid {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}
	c.Next()
}

func CreateAccessToken(cNroDni string) (string, error) {
	expirationTime := time.Now().Add(5 * time.Second)
	claims := &Claims{
		NroDni: cNroDni,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(jwtKey)
}

func CreateRefreshToken(c *gin.Context, cNroDni string) (string, int, error) {
	expirationTime := time.Now().Add(15 * time.Second)
	claims := &Claims{
		NroDni: cNroDni,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tknstr, err := token.SignedString(jwtKey)
	timeout := int(expirationTime.Unix())
	return tknstr, timeout, err

}

func Correlativo(str string) string {
	lcCodigo := []rune(str)
	i := len(str) - 1
	for i >= 0 {
		lcDigito := lcCodigo[i]
		if lcDigito == '9' {
			lcDigito = 'A'
		} else if lcDigito < '9' {
			lcDigito = rune(int(lcDigito) + 1)
		} else if lcDigito < 'Z' {
			lcDigito = rune(int(lcDigito) + 1)
		} else if lcDigito == 'Z' {
			lcDigito = '0'
		}
		lcCodigo[i] = lcDigito
		if lcDigito != '0' {
			break
		}
		i -= 1
	}
	return string(lcCodigo)
}
