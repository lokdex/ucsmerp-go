package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func Refresh(c *gin.Context) {
	code, claims := ValidateRefreshToken(c)
	if code != 0 {
		c.AbortWithStatus(code)
		return
	}
	accTkn, err := CreateAccessToken(claims.NroDni)
	if err != nil {
		c.String(http.StatusInternalServerError, "ERROR GENERANDO ACCESS TOKEN")
		return
	}

	refresh, timeout, err := CreateRefreshToken(c, claims.NroDni)
	if err != nil {
		c.String(http.StatusInternalServerError, "ERROR GENERANDO REFRESH TOKEN")
		return
	}

	c.SetCookie("UCSM", refresh, timeout, "/", "", true, true)
	c.String(http.StatusOK, accTkn)
}
