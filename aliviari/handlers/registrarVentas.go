package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx/v4"
)

type DetVen struct {
	CCODART string
	NCANTID float64
	NMONTO  float64
	NMONIGV float64
	NMONDOL float64
}

type Venta struct {
	CIDFACT int32
	CIDVENT string
	CTIPCOM string
	CNROVEN string
	DFECHA  string
	CNRORUC string
	CESTADO string
	CMONEDA string
	NMONTO  float64
	NMONIGV float64
	NMONDOL float64
	DATOS   []DetVen
}

func RegistrarVentas(c *gin.Context) {
	var laVentas []Venta
	logStr := ""
	resp, err := http.Post("http://localhost:9092/ventas", "application/json", c.Request.Body)
	if err != nil {
		c.String(http.StatusInternalServerError, "ERROR RECUPERANDO VENTAS")
		return
	}
	err = json.NewDecoder(resp.Body).Decode(&laVentas)
	if err != nil {
		c.String(http.StatusBadRequest, "ERROR AL LEER REQUEST")
	}
	for i := 0; i < len(laVentas); i++ {
		laFila := &laVentas[i]
		if len(laFila.DATOS) == 0 && laFila.CESTADO != "X" {
			c.String(http.StatusBadRequest, fmt.Sprintf("CABECERA NO TIENE DETALLE: %v - %v", laFila.CNRORUC, laFila.CNROVEN))
			return
		}
		lcSql := `SELECT cIdVent FROM E02MVEN WHERE cNroVen = $1`
		err := ERPPool.QueryRow(context.Background(), lcSql, laFila.CNROVEN).Scan(&laFila.CIDVENT)
		if err == nil {
			lcSql = `DELETE FROM E02DVEN WHERE cIdVent = $1`
			_, err := ERPPool.Exec(context.Background(), lcSql, laFila.CIDVENT)
			if err != nil {
				c.String(http.StatusInternalServerError, "ERROR AL ELIMINAR DETALLE ANTERIOR")
				return
			}
		} else if err != pgx.ErrNoRows {
			c.String(http.StatusInternalServerError, "ERROR VERIFICANDO REGISTRO PREVIO")
			return
		}
		lcSql = `SELECT setval(pg_get_serial_sequence('E02DVEN', 'nserial'), coalesce(max(nSerial), 0) + 1, false)
					FROM E02DVEN;`
		_, err = ERPPool.Exec(context.Background(), lcSql)
		if err != nil {
			c.String(http.StatusInternalServerError, "ERROR AL RENUMERAR SERIAL")
			return
		}
		lnMonto, lnMonIgv, lnMonDol := 0.0, 0.0, 0.0
		for j := 0; j < len(laFila.DATOS); j++ {
			laTmp := &laFila.DATOS[j]
			lnMonto += laTmp.NMONTO
			lnMonIgv += laTmp.NMONIGV
			lnMonDol += laTmp.NMONDOL
			lcSql = `SELECT COUNT(cDescri) FROM E01MART WHERE cCodArt = $1`
			count := 0
			err = ERPPool.QueryRow(context.Background(), lcSql, laTmp.CCODART).Scan(&count)
			if err != nil {
				c.String(http.StatusInternalServerError, "ERROR VALIDANDO CÓDIGO DE ARTÍCULO")
				return
			} else if count == 0 {
				logStr += fmt.Sprintf("ERR04;ARTICULO NO REGISTRADO EN ERP;%v;\n", laTmp.CCODART)
			}
		}
		if math.Round(lnMonto*100)/100 != math.Round((laFila.NMONTO)*100)/100 {
			logStr += fmt.Sprintf("ERR11;SUMATORIA DE MONTO;%v;%v;\n", laFila.CNRORUC, laFila.CNROVEN)
		}
		if math.Round(lnMonIgv*100)/100 != math.Round(laFila.NMONIGV*100)/100 {
			logStr += fmt.Sprintf("ERR12;SUMATORIA DE IGV;%v;%v;\n", laFila.CNRORUC, laFila.CNROVEN)
		}
		if math.Round(lnMonDol*100)/100 != math.Round(laFila.NMONDOL*100)/100 {
			logStr += fmt.Sprintf("ERR13;SUMATORIA DE DOL;%v;%v;\n", laFila.CNRORUC, laFila.CNROVEN)
		}
	}
	registrarVentas(c, laVentas)
	f, _ := os.OpenFile("AliviariVentas.csv", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	_, _ = f.WriteString(logStr)
	f.Close()
}

func registrarVentas(c *gin.Context, laVentas []Venta) {
	for i := 0; i < len(laVentas); i++ {
		laFila := &laVentas[i]
		if laFila.NMONTO == 0 {
			continue
		}
		if laFila.CIDVENT == "" {
			lcSql := `SELECT MAX(cIdVent) FROM E02MVEN`
			err := ERPPool.QueryRow(context.Background(), lcSql).Scan(&laFila.CIDVENT)
			if err != nil {
				laFila.CIDVENT = "00001"
			}
			laFila.CIDVENT = Correlativo(laFila.CIDVENT)
			if laFila.CNRORUC == "" {
				laFila.CNRORUC = "00000000000"
			}
			lcSql = `INSERT INTO E02MVEN (cIdVent, cTipCom, cNroVen, cTipo, dFecha, cNroRuc, cDescri,
										  cEstado, cMoneda, nMonto, nMonIgv, cUsuCod)
								VALUES ($1, $2, $3, 'V', $4, $5, '', 'A', $6, $7, $8, 'Q103')`
			_, err = ERPPool.Exec(context.Background(), lcSql,
				laFila.CIDVENT, laFila.CTIPCOM, laFila.CNROVEN, laFila.DFECHA, laFila.CNRORUC,
				laFila.CMONEDA, laFila.NMONTO, laFila.NMONIGV)
			if err != nil {
				c.String(http.StatusInternalServerError, "ERROR AL INSERTAR CABECERA DE VENTA")
				return
			}
		} else {
			lcSql := `UPDATE E02MVEN SET (cTipCom, dFecha, cEstado, cMoneda, nMonto, nMonIgv, cUsuCod) =
										($1, $2, 'A', $3, $4, $5, 'Q103')
							WHERE cIdVent = $6`
			_, err := ERPPool.Exec(context.Background(), lcSql, laFila.CTIPCOM, laFila.DFECHA,
				laFila.CMONEDA, laFila.NMONTO, laFila.NMONIGV, laFila.CIDVENT)
			if err != nil {
				c.String(http.StatusInternalServerError, "ERROR AL ACTUALIZAR CABECERA DE VENTA")
				return
			}
		}
		for j := 0; j < len(laFila.DATOS); j++ {
			laTmp := &laFila.DATOS[j]
			lcSql := `SELECT COUNT(cDescri) FROM E01MART WHERE cCodArt = $1`
			count := 0
			err := ERPPool.QueryRow(context.Background(), lcSql, laTmp.CCODART).Scan(&count)
			if err != nil {
				c.String(http.StatusInternalServerError, "ERROR VALIDANDO CÓDIGO DE ARTÍCULO")
				return
			} else if count == 0 {
				continue
			}
			lcSql = `INSERT INTO E02DVEN (cIdVent, cCodArt, nCantid, nMonto, nMonIgv, cUsuCod)
								VALUES ($1, $2, $3, $4, $5, 'Q103')`
			_, err = ERPPool.Exec(context.Background(), lcSql, laFila.CIDVENT, laTmp.CCODART, laTmp.NCANTID, laTmp.NMONTO, laTmp.NMONIGV)
			if err != nil {
				c.String(http.StatusInternalServerError, "ERROR AL INSERTAR DETALLE DE VENTA")
				return
			}
		}
	}
	c.AbortWithStatus(http.StatusOK)
}
