package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx/v4"
)

type DetCom struct {
	IDKARD  int32
	CCODART string
	NCANTID int16
	NMONTO  float64
	NMONIGV float64
	NMONDOL float64
}

type Compra struct {
	IDKARD  int32
	CIDCOMP string
	DFECHA  time.Time
	CNROCOM string
	CNRORUC string
	CTIPCOM string
	CDESCRI string
	CMONEDA string
	NMONTO  float64
	NMONIGV float64
	NMONDOL float64
	DATOS   []DetCom
}

func RegistrarCompras(c *gin.Context) {
	var laCompras []Compra
	logStr := ""
	resp, err := http.Post("http://localhost:9092/compras", "application/json", c.Request.Body)
	if err != nil {
		c.String(http.StatusInternalServerError, "ERROR RECUPERANDO COMPRAS")
		return
	}
	err = json.NewDecoder(resp.Body).Decode(&laCompras)
	if err != nil {
		c.String(http.StatusBadRequest, "ERROR AL LEER REQUEST")
		return
	}
	for i := 0; i < len(laCompras); i++ {
		laFila := &laCompras[i]
		if len(laFila.DATOS) == 0 {
			c.String(http.StatusBadRequest, fmt.Sprintf("CABECERA NO TIENE DETALLE: %v - %v", laFila.CNRORUC, laFila.CNROCOM))
			return
		}
		lcSql := `SELECT cIdComp FROM E02MCOM WHERE cNroCom = $1 AND cNroRuc = $2`
		err := ERPPool.QueryRow(context.Background(), lcSql, laFila.CNROCOM, laFila.CNRORUC).Scan(&laFila.CIDCOMP)
		if err == nil {
			lcSql = `DELETE FROM E02DCOM WHERE cIdComp = $1`
			_, err := ERPPool.Exec(context.Background(), lcSql, laFila.CIDCOMP)
			if err != nil {
				c.String(http.StatusInternalServerError, "ERROR AL ELIMINAR DETALLE ANTERIOR")
				return
			}
		} else if err != pgx.ErrNoRows {
			c.String(http.StatusInternalServerError, "ERROR VERIFICANDO REGISTRO PREVIO")
			return
		}
		lcSql = `SELECT setval(pg_get_serial_sequence('E02DCOM', 'nserial'), coalesce(max(nSerial), 0) + 1, false)
					FROM E02DCOM;`
		_, err = ERPPool.Exec(context.Background(), lcSql)
		if err != nil {
			c.String(http.StatusInternalServerError, "ERROR AL RENUMERAR SERIAL")
			return
		}
		lnMonto, lnMonIgv, lnMonDol := 0.0, 0.0, 0.0
		for j := 0; j < len(laFila.DATOS); j++ {
			laTmp := &laFila.DATOS[j]
			lnMonto += laTmp.NMONTO
			lnMonIgv += laTmp.NMONIGV
			lnMonDol += laTmp.NMONDOL
			lcSql = `SELECT COUNT(cDescri) FROM E01MART WHERE cCodArt = $1`
			count := 0
			err = ERPPool.QueryRow(context.Background(), lcSql, laTmp.CCODART).Scan(&count)
			if err != nil {
				c.String(http.StatusInternalServerError, "ERROR VALIDANDO CÓDIGO DE ARTÍCULO")
				return
			} else if count == 0 {
				logStr += fmt.Sprintf("ERR04;ARTICULO NO REGISTRADO EN ERP;%v;\n", laTmp.CCODART)
			}
		}
		if math.Round(lnMonto*100)/100 != math.Round((laFila.NMONTO-laFila.NMONIGV)*100)/100 {
			logStr += fmt.Sprintf("ERR01;SUMATORIA DE MONTO;%v;%v;\n", laFila.CNRORUC, laFila.CNROCOM)
		}
		if math.Round(lnMonIgv*100)/100 != math.Round(laFila.NMONIGV*100)/100 {
			logStr += fmt.Sprintf("ERR02;SUMATORIA DE IGV;%v;%v;\n", laFila.CNRORUC, laFila.CNROCOM)
		}
		if math.Round(lnMonDol*100)/100 != math.Round(laFila.NMONDOL*100)/100 {
			logStr += fmt.Sprintf("ERR03;SUMATORIA DE DOL;%v;%v;\n", laFila.CNRORUC, laFila.CNROCOM)
		}
	}
	registrarCompras(c, laCompras)
	f, _ := os.OpenFile("AliviariCompras.csv", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	_, _ = f.WriteString(logStr)
	f.Close()
}

func registrarCompras(c *gin.Context, laCompras []Compra) {
	for i := 0; i < len(laCompras); i++ {
		laFila := &laCompras[i]
		if laFila.NMONTO == 0 {
			continue
		}
		if laFila.CIDCOMP == "" {
			lcSql := `SELECT MAX(cIdComp) FROM E02MCOM`
			err := ERPPool.QueryRow(context.Background(), lcSql).Scan(&laFila.CIDCOMP)
			if err != nil {
				if err == pgx.ErrNoRows {
					laFila.CIDCOMP = "00001"
				} else {
					c.String(http.StatusInternalServerError, "ERROR RECUPERANDO ÚLTIMO ID")
					return
				}
			}
			laFila.CIDCOMP = Correlativo(laFila.CIDCOMP)
			lcSql = `INSERT INTO E02MCOM (cIdComp, dFecha, cNroCom, cNroRuc, cTipCom, cDescri,
										  cEstado, cMoneda, nMonto, nMonIgv, cUsuCod)
								VALUES ($1, $2, $3, $4, $5, $6, 'A', $7, $8, $9, 'Q103')`
			_, err = ERPPool.Exec(context.Background(), lcSql,
				laFila.CIDCOMP, laFila.DFECHA, laFila.CNROCOM, laFila.CNRORUC, laFila.CTIPCOM,
				laFila.CDESCRI, laFila.CMONEDA, laFila.NMONTO, laFila.NMONIGV)
			if err != nil {
				c.String(http.StatusInternalServerError, "ERROR AL INSERTAR CABECERA DE COMPRA")
				return
			}
		} else {
			lcSql := `UPDATE E02MCOM SET (dFecha, cTipCom, cDescri, cEstado, cMoneda, nMonto, nMonIgv, cUsuCod) =
										($1, $2, $3, 'A', $4, $5, $6, 'Q103')
							WHERE cIdComp = $7`
			_, err := ERPPool.Exec(context.Background(), lcSql, laFila.DFECHA, laFila.CTIPCOM, laFila.CDESCRI,
				laFila.CMONEDA, laFila.NMONTO, laFila.NMONIGV, laFila.CIDCOMP)
			if err != nil {
				c.String(http.StatusInternalServerError, "ERROR AL ACTUALIZAR CABECERA DE COMPRA")
				return
			}
		}
		for j := 0; j < len(laFila.DATOS); j++ {
			laTmp := &laFila.DATOS[j]
			lcSql := `SELECT COUNT(cDescri) FROM E01MART WHERE cCodArt = $1`
			count := 0
			err := ERPPool.QueryRow(context.Background(), lcSql, laTmp.CCODART).Scan(&count)
			if err != nil {
				c.String(http.StatusInternalServerError, "ERROR VALIDANDO CÓDIGO DE ARTÍCULO")
				return
			} else if count == 0 {
				continue
			}
			lcSql = `INSERT INTO E02DCOM (cIdComp, cCodArt, nCantid, nMonto, nMonIgv, cUsuCod)
							VALUES ($1, $2, $3, $4, $5, 'Q103')`
			_, err = ERPPool.Exec(context.Background(), lcSql, laFila.CIDCOMP, laTmp.CCODART, laTmp.NCANTID, laTmp.NMONTO, laTmp.NMONIGV)
			if err != nil {
				c.String(http.StatusInternalServerError, "ERROR AL INSERTAR DETALLE DE COMPRA")
				return
			}
		}
	}
	c.AbortWithStatus(http.StatusOK)
}
