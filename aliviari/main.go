package main

import (
	"aliviari/handlers"
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {
	gin.SetMode(gin.ReleaseMode)
	r := gin.New()
	r.Use(gin.Recovery())
	r.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"http://localhost:3001"},
		AllowMethods:     []string{"POST", "DELETE", "OPTIONS"},
		AllowHeaders:     []string{"Origin", "Content-Type", "X-Auth-Token", "Authorization", "X-Requested-With", "Set-Cookie"},
		AllowCredentials: true,
	}))

	authorized := r.Group("/")
	authorized.Use(handlers.ValidateToken)
	authorized.POST("/registrarCompras", handlers.RegistrarCompras)
	authorized.POST("/registrarVentas", handlers.RegistrarVentas)
	authorized.GET("/notaCredito", handlers.GetNotaCredito)
	authorized.POST("/notaCredito", handlers.CrearNotaCredito)
	authorized.DELETE("/notaCredito", handlers.AnularNotaCredito)
	authorized.GET("/buscarRUC", handlers.GetComprobantes)
	authorized.GET("/comprobantesNotas", handlers.GetComprobantesNotas)
	authorized.POST("/registrarPago", handlers.RegistrarPago)

	s := http.Server{
		Addr:         ":9091",          // configure the bind address
		Handler:      r,                // set the default handler
		ReadTimeout:  10 * time.Second, // max time to read request from the client
		WriteTimeout: 30 * time.Second, // max time to write response to the client
	}

	// s.SetKeepAlivesEnabled(false)

	go func() {
		fmt.Println("Starting server on port 9091")
		var err error
		handlers.ERPPool, err = handlers.InitDB()
		if err != nil {
			fmt.Printf("ERROR CONECTANDO A LA BASE DE DATOS: %s\n", err)
			os.Exit(1)
		}
		err = s.ListenAndServe()
		if err != nil {
			fmt.Printf("Error starting server: %s\n", err)
			os.Exit(1)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	// Block until a signal is received.
	sig := <-c
	log.Println("Got signal:", sig)
	// gracefully shutdown the server, waiting max 30 seconds for current operations to complete
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	if err := s.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown:", err)
	}
	log.Println("Server shutdown")
}
