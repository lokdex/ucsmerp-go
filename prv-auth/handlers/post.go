package handlers

import (
	"fmt"
	"pauth/data"

	// "context"
	"crypto/sha512"
	"database/sql"
	"encoding/hex"
	"encoding/json"
	"net/http"
	"os"

	_ "github.com/jackc/pgx/v4/stdlib"
)

// swagger:route POST /iniciar-sesion auth iniciarSesion
// Validar las credenciales del usuario
//
// responses:
// 200: authResponse
//  422: errorValidation
//  500: errorResponse

// IniciarSesion gestiona las solicitudes POST para autenticar un usuario
func (u *User) IniciarSesion(rw http.ResponseWriter, r *http.Request) {
	// ip := r.RemoteAddr
	// xforward := r.Header.Get("X-Forwarded-For")
	// fmt.Println("IP : ", ip)
	// fmt.Println("X-Forwarded-For : ", xforward)
	// fmt.Println("Time : ", time.Now())
	user := r.Context().Value(KeyUser{}).(*data.Usuario)
	defer r.Body.Close()

	cclave := sha512.New()
	cclave.Write([]byte(user.Clave))
	cclahex := hex.EncodeToString(cclave.Sum(nil))

	// dbpool, err := pgx.Connect(context.Background(), os.Getenv("DB_ERP"))
	dbpool, err := sql.Open("pgx", os.Getenv("DB_ERP"))
	// dbpool, err := sql.Open("pgx", "postgres://postgres:postgres@localhost:5432/UCSMERP")
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(rw).Encode(&GenericError{Message: "Error al conectar la base de datos"})
		return
	}

	// Validar documento de identidad en la DB
	query := `SELECT cNroDni, cNombre, cClave, cClaAca FROM S01MPER WHERE cEstado = 'A' AND `
	if len(user.NroDni) == 8 {
		query += `cNroDni = $1`
	} else {
		query += `cNroDoc = $1`
	}
	err = dbpool.QueryRow(query, user.NroDni).Scan(&user.NroDni, &user.Nombre, &user.Clave, &user.ClaAca)
	if err == sql.ErrNoRows {
		rw.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(rw).Encode(&GenericError{Message: "No se encontró documento de identidad"})
		return
	} else if err != nil {
		fmt.Print("1")
		rw.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(rw).Encode(&GenericError{Message: "Error de ejecución de base de datos"})
		return
	}
	// Validar si el usuario esta asignado y/o activo
	err = dbpool.QueryRow(`SELECT cEstado, cCodUsu, TRIM(cCargo), cNivel
						FROM V_S01TUSU_1 WHERE cNroDni = $1 AND cEstado = 'A';`, user.NroDni).Scan(
		&user.Estado, &user.CodUsu, &user.Cargo, &user.Nivel,
	)
	if err == sql.ErrNoRows {
		rw.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(rw).Encode(&GenericError{Message: "Usuario no asignado o no está activo"})
		return
	} else if err != nil {
		fmt.Print("2")
		rw.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(rw).Encode(&GenericError{Message: "Error de ejecución de base de datos"})
		return
	}
	// Validar contraseña del usuario
	err = dbpool.QueryRow(`SELECT cCodDoc FROM A01MDOC WHERE cCodDoc = $1 AND cEstado = 'A'`, user.CodUsu).Scan()
	lbAuth := false
	if err != nil {
		if err != sql.ErrNoRows {
			fmt.Print("3")
			rw.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(rw).Encode(&GenericError{Message: "Error de ejecución de base de datos"})
			return
		} else if err == sql.ErrNoRows {
			lbAuth = cclahex == user.Clave
		} else {
			lbAuth = cclahex == user.ClaAca
		}
	}
	if !lbAuth {
		rw.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(rw).Encode(&GenericError{Message: "Contraseña incorrecta"})
		return
	}
	// Recuperar centro de costo asignado al usuario
	err = dbpool.QueryRow(`SELECT cCenCos FROM S01PCCO WHERE cCodUsu = $1 AND cEstado = 'A' ORDER BY cCenCos LIMIT 1`,
		user.CodUsu).Scan(&user.CenCos)
	if err == sql.ErrNoRows {
		user.CenCos = "*"
	} else if err != nil {
		fmt.Print("4")
		rw.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(rw).Encode(&GenericError{Message: "Error de ejecución de base de datos"})
		return
	}
	// Recuperar descripción del centro de costo
	err = dbpool.QueryRow(`SELECT cDescri FROM S01TCCO WHERE cCenCos = $1`, user.CenCos).Scan(&user.DesCco)
	if err != nil {
		fmt.Print("5")
		rw.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(rw).Encode(&GenericError{Message: "Error de ejecución de base de datos"})
		return
	}
	dbpool.Close()

	user.Clave = ""
	user.ClaAca = ""

	http.SetCookie(rw, &http.Cookie{
		Name:   "UCSM",
		Value:  "hello",
		MaxAge: 3600,
	})
	err = json.NewEncoder(rw).Encode(user)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(rw).Encode(&GenericError{Message: "Error codificando respuesta"})
		return
	}
}

// swagger:route POST /init-menu auth initMenu
// Recupera los elementos del menu de acuerdo al código de usuario
//
// responses:
// 200: initResponse
//  422: errorValidation
//  500: errorResponse

// InitMenu recupera las opciones a las que tiene acceso el usuario
func InitMenu(rw http.ResponseWriter, r *http.Request) {
	type Params struct {
		CodUsu string `json:"CCODUSU"`
	}
	params := Params{}

	err := json.NewDecoder(r.Body).Decode(&params)
	defer r.Body.Close()

	dbpool, err := sql.Open("pgx", os.Getenv("DB_ERP"))
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(rw).Encode(&GenericError{Message: "Error al conectar la base de datos"})
		return
	}
	defer dbpool.Close()

	// Roles asignados a usuario
	rows, err := dbpool.Query(`SELECT DISTINCT cCodRol, cDesRol FROM V_S01TUSU_2
										WHERE cCodUsu = $1 AND cEstRol = 'A' ORDER BY cCodRol`, params.CodUsu)
	if err != nil {
		fmt.Printf("db error %v", err)
		rw.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(rw).Encode(&GenericError{Message: "Error de ejecución de base de datos"})
		return
	}

	type role struct {
		CodRol string
		DesRol string
	}
	userRoles := []role{}

	for rows.Next() {
		rol := role{}
		if err := rows.Scan(&rol.CodRol, &rol.DesRol); err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(rw).Encode(&GenericError{Message: "Error leyendo respuesta de la base de datos."})
			return
		}
		userRoles = append(userRoles, rol)
	}

	// Opciones asignadas a usuario
	rows, err = dbpool.Query(`SELECT cCodRol, cDesRol, cCodOpc, cDesOpc FROM V_S01TUSU_2
									  WHERE cCodUsu = $1 AND cEstOpc = 'A' ORDER BY cCodRol, nOrden, cCodOpc`, params.CodUsu)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(rw).Encode(&GenericError{Message: "Error de ejecución de base de datos"})
		return
	}

	type options struct {
		CodRol string
		DesRol string
		CodOpc string
		DesOpc string
	}
	userOpc := []options{}

	for rows.Next() {
		opc := options{}
		if err := rows.Scan(&opc.CodRol, &opc.DesRol, &opc.CodOpc, &opc.DesOpc); err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(rw).Encode(&GenericError{Message: "Error leyendo respuesta de la base de datos."})
			return
		}
		userOpc = append(userOpc, opc)
	}

	// Periodo activo del sistema
	var period string
	err = dbpool.QueryRow("SELECT cPeriod FROM S01TPER WHERE cTipo = 'S0'").Scan(&period)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(rw).Encode(&GenericError{Message: "Error de ejecución de base de datos"})
		return
	}

	type response struct {
		Roles    []role
		Opciones []options
		Periodo  string
	}

	res := &response{
		Roles:    userRoles,
		Opciones: userOpc,
		Periodo:  period,
	}

	err = json.NewEncoder(rw).Encode(res)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(rw).Encode(&GenericError{Message: "Error codificando respuesta"})
		return
	}
}
