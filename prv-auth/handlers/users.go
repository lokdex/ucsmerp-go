package handlers

import (
	"log"

	"pauth/data"
)

type KeyUser struct{}

type User struct {
	l *log.Logger
	v *data.Validation
}

func NewUser(l *log.Logger, v *data.Validation) *User {
	return &User{l, v}
}

// GenericError is a generic error message returned by a server
type GenericError struct {
	Message string `json:"message"`
}

// ValidationError is a collection of validation error messages
type ValidationError struct {
	Messages []string `json:"messages"`
}
