package handlers

import (
	"pauth/data"
	"context"
	"encoding/json"
	"net/http"
)

// MiddlewareValidateProduct validates the product in the request and calls next if ok
func (u *User) MiddlewareValidateUser(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		user := &data.Usuario{}

		// err := utils.FromJSON(user, r.Body)
		err := json.NewDecoder(r.Body).Decode(user)
		if err != nil {
			u.l.Println("[ERROR] deserializing user", err)

			rw.WriteHeader(http.StatusBadRequest)
			// utils.ToJSON(&GenericError{Message: err.Error()}, rw)
			json.NewEncoder(rw).Encode(&GenericError{Message: err.Error()})
			return
		}

		// validate the product
		errs := u.v.Validate(user)
		if len(errs) != 0 {
			u.l.Println("[ERROR] validating user", errs)

			// return the validation messages as an array
			rw.WriteHeader(http.StatusUnprocessableEntity)
			// utils.ToJSON(&ValidationError{Messages: errs.Errors()}, rw)
			json.NewEncoder(rw).Encode(&ValidationError{Messages: errs.Errors()})
			return
		}

		// add the product to the context
		ctx := context.WithValue(r.Context(), KeyUser{}, user)
		r = r.WithContext(ctx)

		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(rw, r)
	})
}
