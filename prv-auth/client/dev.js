const esbuild = require('esbuild');
const http = require('http');

// Start esbuild's server on a random local port
esbuild
  .serve(
    { servedir: './public' },
    {
      entryPoints: ['src/index.js'],
      outdir: './public',
      bundle: true,
      loader: {
        '.js': 'jsx',
        /* '.png': 'dataurl', */
        '.jpg': 'dataurl',
        '.svg': 'dataurl',
        '.woff2': 'file',
        '.woff': 'file',
      },
    }
  )
  .then(result => {
    // console.log(result);
    // console.log("Listening on ", result.host, result.port);
    // The result tells us where esbuild's local server is
    const { host, port } = result;

    // Then start a proxy server on port 3000
    http
      .createServer((req, res) => {
        const options = {
          hostname: host,
          port: port,
          path: req.url,
          method: req.method,
          headers: req.headers,
        };

        // Forward each incoming request to esbuild
        const proxyReq = http.request(options, proxyRes => {
          // If esbuild returns "not found", send a custom 404 page
          // Otherwise, forward the response from esbuild to the client
          res.writeHead(proxyRes.statusCode, proxyRes.headers);
          proxyRes.pipe(res, { end: true });
        });

        // Forward the body of the request to esbuild
        req.pipe(proxyReq, { end: true });
      })
      .listen(3000);
    console.log('Listening on port 3000');
  });
