import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { ChakraProvider } from '@chakra-ui/react';

import theme from './theme.js';
import './styles.css';

import IniciarSesion from './views/IniciarSesion';
import Menu from './views/Menu';

function App() {
  return (
    <ChakraProvider theme={theme}>
      <Router>
        <Routes>
          <Route path="/" element={<IniciarSesion />} />
          <Route path="/menu" element={<Menu />} />
        </Routes>
      </Router>
    </ChakraProvider>
  );
}

export default App;
