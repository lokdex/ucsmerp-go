import { extendTheme, theme as base } from '@chakra-ui/react';

// 2. Call `extendTheme` and pass your custom values
const theme = extendTheme({
  colors: {
    brand: '#05BE6A',
  },
  fonts: {
    heading: `Open Sans, ${base.fonts?.heading}`,
  },
});

export default theme;
