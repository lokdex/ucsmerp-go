import { render, screen } from '@testing-library/react';

import { MemoryRouter } from 'react-router-dom';
import '@testing-library/jest-dom/extend-expect';
import * as React from 'react';

import Menu from '../../views/Menu';

test('Menu test', async function () {
  const Wrapper = ({ children }) => <MemoryRouter>{children}</MemoryRouter>;

  render(<Menu />, { wrapper: Wrapper });

  const heading = screen.getByText(/opciones/i);

  expect(heading).toBeInTheDocument();
});
