import { render, screen, act } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { MemoryRouter } from 'react-router-dom';
import '@testing-library/jest-dom/extend-expect';
import * as React from 'react';

import IniciarSesion from '../../views/IniciarSesion';

test('Log in test', async function () {
  const promise = Promise.resolve();
  const onSubmit = jest.fn();

  const Wrapper = ({ children }) => <MemoryRouter>{children}</MemoryRouter>;
  render(<IniciarSesion onSubmit={onSubmit} />, { wrapper: Wrapper });

  const nrodni = '70405735';
  const password = '70405735';

  userEvent.type(screen.getByLabelText(/documento/i), password);
  userEvent.type(screen.getByLabelText(/contraseña/i), password);

  userEvent.click(screen.getByRole('button', { name: /ingresar/i }));

  expect(onSubmit).toHaveBeenCalledWith({ nrodni, password });

  await act(() => promise);
});
