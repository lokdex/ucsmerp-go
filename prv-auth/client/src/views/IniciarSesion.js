import React from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import {
  Box,
  Grid,
  Container,
  Center,
  FormControl,
  FormLabel,
  Input,
  Button,
  Image,
  Heading,
  Divider,
} from '@chakra-ui/react';

import Background from '../images/background.jpg';
import Imagotipo from '../images/ucsm-imagotipo-color.svg';

const IniciarSesion = ({ onSubmit }) => {
  const navigate = useNavigate();

  const iniciarSesion = async e => {
    e.preventDefault();

    const { nrodni, password } = e.target.elements;

    let nrodni_val = nrodni.value;
    let password_val = password.value;

    // onSubmit({
    //   nrodni: nrodni_val,
    //   password: password_val,
    // });

    axios
      .post(
        'http://localhost:9090/iniciar-sesion',
        JSON.stringify({
          CNRODNI: nrodni_val,
          CCLAVE: password_val,
        })
      )
      .then(res => {
        if (res.data) {
          sessionStorage.setItem('ERP_user_data', JSON.stringify(res.data));
          navigate('/menu');
        }
      })
      .catch(error => {
        switch (error.response.status) {
          case 400:
            alert(error.response.data.ERROR);
            break;
          case 422:
            console.log(error.response.data.messages);
            let err = JSON.parse(error.response.data.messages);
            alert(err.Error);
            break;
          case 500:
            alert(error.response.data.message);
            break;
          default:
            alert('Error en la solicitud');
        }
      });
  };
  return (
    <div
      style={{
        backgroundImage: `url(${Background})`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundSize: 'cover',
      }}
    >
      <Box textAlign="center" fontSize="xl">
        <Grid minH="100vh" p={3}>
          <Center spacing={8}>
            <Container maxW="container.sm" borderRadius="md" bg="white" p={5}>
              <Image src={Imagotipo} mx="auto" />
              <Heading>Sistema ERP</Heading>
              <Divider />
              <form onSubmit={iniciarSesion}>
                <FormControl>
                  <FormLabel>Nro. de documento</FormLabel>
                  <Input name="nrodni" type="text" />
                </FormControl>
                <FormControl>
                  <FormLabel>Contraseña</FormLabel>
                  <Input name="password" type="password" />
                </FormControl>
                <Button
                  type="submit"
                  bgColor="brand"
                  color="white"
                  variant="solid"
                  mt={3}
                  w="50%"
                >
                  Ingresar
                </Button>
              </form>
            </Container>
          </Center>
        </Grid>
      </Box>
    </div>
  );
};

export default IniciarSesion;
