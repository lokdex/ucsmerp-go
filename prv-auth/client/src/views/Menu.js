import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import {
  Box,
  Grid,
  Container,
  Center,
  FormControl,
  FormLabel,
  Input,
  Text,
  Button,
  Image,
  Heading,
  Divider,
  Flex,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
} from '@chakra-ui/react';

import Imagotipo from '../images/ucsm-imagotipo-blanco.svg';

const Menu = () => {
  let navigate = useNavigate();
  let [userData, setUserData] = useState(null);
  let [user, setUser] = useState(null);

  const initMenu = () => {
    let ccodusu = JSON.parse(sessionStorage.getItem('ERP_user_data')).CCODUSU;
    axios
      .post(
        'http://localhost:9090/init-menu',
        JSON.stringify({ CCODUSU: ccodusu })
      )
      .then(res => {
        if (res.data) {
          console.log(res.data);
          setUserData(res.data);
        }
      })
      .catch(error => {
        switch (error.response.status) {
          case 400:
            alert(error.response.data.ERROR);
            break;
          case 422:
            console.log(error.response.data.messages);
            let err = JSON.parse(error.response.data.messages);
            alert(err.Error);
            break;
          case 500:
            alert(error.response.data.message);
            break;
          default:
            alert('Error en la solicitud');
        }
      });
  };

  useEffect(() => {
    let user = sessionStorage.getItem('ERP_user_data');
    if (user === null) {
      navigate('/');
    } else {
      setUser(JSON.parse(user));
      initMenu();
    }
  }, []);

  return (
    <Flex flexDirection="column" h="100vh">
      <Box bg="brand" p="0.5rem">
        <Flex alignItems="center">
          <Image src={Imagotipo} h="3rem" />
          <div style={{ flexGrow: 1 }}></div>
          <Button variant="ghost" _hover={{ bgColor: '#06A95F' }} color="white">
            Menú
          </Button>
          <Button variant="ghost" _hover={{ bgColor: '#06A95F' }} color="white">
            Cerrar Sesión
          </Button>
        </Flex>
      </Box>
      <Box textAlign="center" fontSize="xl" flexGrow="1" p={5}>
        <Box
          maxW="container.xxl"
          borderRadius="md"
          bg="white"
          border="1px solid gray"
        >
          <Box bg="#f8aa1a">
            <Flex alignItems="center">
              <Heading size="md" color="white" py={3} pl={3}>
                Menú de opciones
              </Heading>
              <div style={{ flexGrow: 1 }}></div>
              <Heading size="md" color="white" pr={3}>
                {user?.CCODUSU}
              </Heading>
            </Flex>
          </Box>
          <Box>
            <Accordion>
              {userData?.Roles.map((item, idx) => (
                <AccordionItem key={`item-${idx}`}>
                  <h2>
                    <AccordionButton>
                      <Box flex="1" textAlign="left">
                        <Heading size="md">{item?.DesRol}</Heading>
                      </Box>
                      <AccordionIcon />
                    </AccordionButton>
                  </h2>
                  <AccordionPanel>
                    <Flex flexDirection="column">
                      {userData?.Opciones.map((opc, idx) =>
                        opc?.CodRol === item.CodRol ? (
                          <Button justifyContent="start" variant="outline">
                            {opc?.DesOpc}
                          </Button>
                        ) : (
                          <></>
                        )
                      )}
                    </Flex>
                  </AccordionPanel>
                </AccordionItem>
              ))}
            </Accordion>
          </Box>
        </Box>
      </Box>
      <Divider />
      <Box textAlign="center" m="1rem">
        © 2021 Universidad Católica de Santa María. Todos los derechos
        reservados. UCSM
      </Box>
    </Flex>
  );
};

export default Menu;
