package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"pauth/data"
	"pauth/handlers"
	"time"

	"github.com/go-openapi/runtime/middleware"
	gohandlers "github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

// code for serving teh react app
type spaHandler struct {
	staticPath string
	indexPath  string
}

func (h spaHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// get the absolute path to prevent directory traversal
	path, err := filepath.Abs(r.URL.Path)
	if err != nil {
		// if we failed to get the absolute path respond with a 400 bad request
		// and stop
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// prepend the path with the path to the static directory
	path = filepath.Join(h.staticPath, path)

	// check whether a file exists at the given path
	_, err = os.Stat(path)
	if os.IsNotExist(err) {
		// file does not exist, serve index.html
		http.ServeFile(w, r, filepath.Join(h.staticPath, h.indexPath))
		return
	} else if err != nil {
		// if we got an error (that wasn't that the file doesn't exist) stating the
		// file, return a 500 internal server error and stop
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// otherwise, use http.FileServer to serve the static dir
	http.FileServer(http.Dir(h.staticPath)).ServeHTTP(w, r)
}

func main() {
	l := log.New(os.Stdout, "UCSMERP-api ", log.LstdFlags)
	v := data.NewValidation()

	// Crear los handlers para las rutas
	authH := handlers.NewUser(l, v)

	sm := mux.NewRouter()

	postR := sm.Methods(http.MethodPost).Subrouter()
	postR.HandleFunc("/iniciar-sesion", authH.IniciarSesion)
	postR.Use(authH.MiddlewareValidateUser)

	aPostR := sm.Methods(http.MethodPost).Subrouter()
	aPostR.HandleFunc("/init-menu", handlers.InitMenu)

	getR := sm.Methods(http.MethodGet).Subrouter()

	// handler for documentation
	opts := middleware.RedocOpts{SpecURL: "/swagger.yaml"}
	sh := middleware.Redoc(opts, nil)

	getR.Handle("/docs", sh)
	getR.Handle("/swagger.yaml", http.FileServer(http.Dir("./")))

	spa := spaHandler{staticPath: "client/public", indexPath: "index.html"}
	getR.PathPrefix("/").Handler(spa)

	// CORS
	methods := gohandlers.AllowedMethods([]string{"POST", "OPTIONS"})
	headers := gohandlers.AllowedHeaders([]string{"Origin", "Content-Type", "X-Auth-Token", "Authorization", "X-Requested-With"})

	origins := gohandlers.AllowedOrigins([]string{"*"})
	ch := gohandlers.CORS(methods, origins, headers)

	s := http.Server{
		Addr:         ":9090",          // configure the bind address
		Handler:      ch(sm),           // set the default handler
		ErrorLog:     l,                // set the logger for the server
		ReadTimeout:  5 * time.Second,  // max time to read request from the client
		WriteTimeout: 10 * time.Second, // max time to write response to the client
		// IdleTimeout:  120 * time.Second, // max time for connections using TCP Keep-Alive
	}

	s.SetKeepAlivesEnabled(false)

	go func() {
		l.Println("Starting server on port 9090")
		err := s.ListenAndServe()
		if err != nil {
			l.Printf("Error starting server: %s\n", err)
			os.Exit(1)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, os.Kill)

	// Block until a signal is received.
	sig := <-c
	log.Println("Got signal:", sig)

	// gracefully shutdown the server, waiting max 30 seconds for current operations to complete
	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
	s.Shutdown(ctx)
}
