package data

import (
	"testing"
)

func TestUserDNILength(t *testing.T) {
	u := Usuario{
		NroDni: "123",
		Clave:  "abc",
	}

	v := NewValidation()
	err := v.Validate(u)
	if len(err) > 1 {
		t.Errorf("Got error: %v", err)
	}
}

func TestMissingDNI(t *testing.T) {
	u := Usuario{
		Clave: "abc",
	}

	v := NewValidation()
	err := v.Validate(u)
	if len(err) > 1 {
		t.Errorf("Got error: %v", err)
	}
}

func TestMissingClave(t *testing.T) {
	u := Usuario{
		NroDni: "12345678",
	}

	v := NewValidation()
	err := v.Validate(u)
	if len(err) > 1 {
		t.Errorf("Got error: %v", err)
	}
}

func TestMissingBothParams(t *testing.T) {
	u := Usuario{}

	v := NewValidation()
	err := v.Validate(u)
	if len(err) != 2 {
		t.Errorf("Got error: %v", err)
	}
}

func TestCorrectParams(t *testing.T) {
	u := Usuario{
		NroDni: "12345678",
		Clave:  "123",
	}

	v := NewValidation()
	err := v.Validate(u)
	if err != nil {
		t.Errorf("Got error: %v", err)
	}
}
