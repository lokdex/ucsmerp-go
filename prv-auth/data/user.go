package data

import (
	"fmt"
)

// ErrUserNotFound es un error que indica que el usuairo no se encontró en la base de datos
var ErrUserNotFound = fmt.Errorf("No se encontró usuario")

// ErrUserInactive es un error que indica que el estado del usuario es nulo
var ErrUserInactive = fmt.Errorf("Usuario no asignado o no está activo")

// ErrWrongPassword es un error que indica que la contraseña no es correcta
var ErrWrongPassword = fmt.Errorf("Contraseña errónea")

// Usuario define la estructura del usuario del ERP
//
// Un usuario es el personal administrativo de la UCSM,
// quienes de acuerdo a su rol pueden interactuar con
// los diferentes módulos del ERP
//
// swagger:model
type Usuario struct {
	// DNI del usuario
	// required: true
	// min length: 8
	// max length: 8
	NroDni string `json:"CNRODNI" validate:"longitud-dni"`

	// Estado del usuario
	// read only: true
	// length: 1
	Estado string `json:"CESTADO"`

	// Nombre del usuario
	// read only: true
	Nombre string `json:"CNOMBRE"`

	// Código del usuario
	// read only: true
	// min length: 4
	// max length: 4
	CodUsu string `json:"CCODUSU"`

	// Código del centro de costo asignado al usuario
	// read only: true
	// min length: 3
	// max length: 3
	CenCos string `json:"CCENCOS"`

	// Descripción del centro de costo asignado al Usuario
	// read only: true
	// max length: 200
	DesCco string `json:"CDESCCO"`

	// Cargo asignado al usuario
	// read only: true
	// min length: 3
	// max length: 3
	Cargo string `json:"CCARGO"`

	// Nivel del usuario
	// read only: true
	// min length: 2
	// max length: 2
	Nivel string `json:"CNIVEL"`

	// Contraseña del usuario
	// read only: true
	// required: true
	Clave string `json:"CCLAVE" validate:"required"`

	// Contraseña del sistema académico del usuario
	// read only: true
	ClaAca string `json:"-"`

	// Campo de error de apoyo para el trabajo con procedimientos almacenados
	// read only: true
	// example: {"ERROR": "CONTRASEÑA INVALIDA"}
	Error string `json:"ERROR"`
}
