package data

import (
	"fmt"

	validator "github.com/go-playground/validator/v10"
)

// ValidationError encapsula el FieldError
// de manera que no se exponga en el codigo
type ValidationError struct {
	validator.FieldError
}

func (v ValidationError) Error() string {
	return fmt.Sprintf(
		`{"Key": "%s", "Error": "Validación de campos para %s falló en %s"}`,
		v.Namespace(),
		v.Field(),
		v.Tag(),
	)
}

// ValidationErrors es una colección de ValidationError
type ValidationErrors []ValidationError

// Errors convierte el slice en un slice de string
func (v ValidationErrors) Errors() []string {
	errs := []string{}
	for _, err := range v {
		errs = append(errs, err.Error())
	}

	return errs
}

// Validation contains
type Validation struct {
	validate *validator.Validate
}

// NewValidation crea un nuevo Validation type
func NewValidation() *Validation {
	validate := validator.New()
	validate.RegisterValidation("longitud-dni", validateDNI)

	return &Validation{validate}
}

// Validar el usuario
func (v *Validation) Validate(i interface{}) ValidationErrors {
	errs := v.validate.Struct(i)
	if errs == nil {
		return nil
	}

	var returnErrs []ValidationError
	for _, err := range errs.(validator.ValidationErrors) {
		// Castear el FieldError a ValidationError y agregarlo al slice
		ve := ValidationError{err}
		returnErrs = append(returnErrs, ve)
	}

	return returnErrs
}

// Validar DNI
func validateDNI(fl validator.FieldLevel) bool {
	dni := fl.Field().String()
	return len(dni) == 8
}
