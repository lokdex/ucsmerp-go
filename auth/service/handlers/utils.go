package handlers

import (
	"context"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Claims struct {
	CodUsu string `json:"CCODUSU"`
	jwt.StandardClaims
}

var ERPPool *pgxpool.Pool

var jwtKey = []byte("INGLORIUS")

func InitDB() (*pgxpool.Pool, error) {
	return pgxpool.Connect(context.Background(), os.Getenv("DB_ERP"))
}

func ValidateAccessToken(c *gin.Context) int {
	auth := c.Request.Header.Get("Authorization")
	auth = strings.Split(auth, " ")[1]
	if auth == "" {
		return http.StatusUnauthorized
	}
	claims := &Claims{}
	token, err := jwt.ParseWithClaims(auth, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			return http.StatusUnauthorized
		}
		return http.StatusForbidden
	}
	if !token.Valid {
		return http.StatusUnauthorized
	}
	return 0
}

func ValidateRefreshToken(c *gin.Context) (int, *Claims) {
	cookie, err := c.Cookie("UCSM")
	if err != nil {
		if err == http.ErrNoCookie {
			return http.StatusUnauthorized, nil
		}
		return http.StatusBadRequest, nil
	}
	claims := &Claims{}
	token, err := jwt.ParseWithClaims(cookie, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			return http.StatusUnauthorized, nil
		}
		return http.StatusBadRequest, nil
	}
	if !token.Valid {
		return http.StatusUnauthorized, nil
	}
	return 0, claims
}

func CreateAccessToken(cCodUsu string) (string, error) {
	expirationTime := time.Now().Add(5 * time.Minute)
	claims := &Claims{
		CodUsu: cCodUsu,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(jwtKey)
}

func CreateRefreshToken(c *gin.Context, cCodUsu string) (string, int, error) {
	expirationTime := time.Now().Add(15 * time.Second)
	claims := &Claims{
		CodUsu: cCodUsu,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tknstr, err := token.SignedString(jwtKey)
	timeout := int(expirationTime.Unix())
	return tknstr, timeout, err

}
