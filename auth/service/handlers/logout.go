package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func Logout(c *gin.Context) {
	c.SetCookie("UCSM", "", -1, "/", "", true, true)
	c.AbortWithStatus(http.StatusOK)
}
