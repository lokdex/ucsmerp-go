package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
)

func Refresh(c *gin.Context) {
	code, claims := ValidateRefreshToken(c)
	if code != 0 {
		c.AbortWithStatus(code)
		return
	}
	accTkn, err := CreateAccessToken(claims.CodUsu)
	if err != nil {
		c.String(http.StatusInternalServerError, "ERROR GENERANDO ACCESS TOKEN")
		return
	}

	refresh, timeout, err := CreateRefreshToken(c, claims.CodUsu)
	if err != nil {
		c.String(http.StatusInternalServerError, "ERROR GENERANDO REFRESH TOKEN")
		return
	}

	lcSql := `SELECT DISTINCT cCodRol, cDesRol FROM V_S01TUSU_2	WHERE cCodUsu = $1 AND cEstRol = 'A' ORDER BY cCodRol`
	rows, err := ERPPool.Query(context.Background(), lcSql, claims.CodUsu)
	if err != nil {
		c.String(http.StatusInternalServerError, "ERROR RECUPERANDO ROLES")
		return
	}

	type role struct {
		CodRol string
		DesRol string
	}
	userRoles := []role{}

	for rows.Next() {
		rol := role{}
		if err := rows.Scan(&rol.CodRol, &rol.DesRol); err != nil {
			c.String(http.StatusInternalServerError, "ERROR LEYENDO RESPUESTA DE LA BASE DE DATOS")
			return
		}
		userRoles = append(userRoles, rol)
	}

	type res struct {
		AccTkn string
		Roles  []role
		CodUsu string
	}
	response := res{
		AccTkn: accTkn,
		Roles:  userRoles,
		CodUsu: claims.CodUsu,
	}
	c.SetCookie("UCSM", refresh, timeout, "/", "", true, true)
	c.JSON(http.StatusOK, response)
}
