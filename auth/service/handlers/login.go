package handlers

import (
	"context"

	// "context"
	"crypto/sha512"
	"database/sql"
	"encoding/hex"
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx/v4"
)

type User struct {
	NroDni string `json:"CNRODNI"`
	Clave  string `json:"CCLAVE"`
	Nombre string `json:"CNOMBRE"`
	ClaAca string `json:"CCLAACA"`
	Estado string `json:"CESTADO"`
	Cargo  string `json:"CCARGO"`
	CodUsu string `json:"CCODUSU"`
	Nivel  string `json:"CNIVEL"`
	CenCos string `json:"CCENCOS"`
	DesCco string `json:"CDESCCO"`
	AccTkn string `json:"Access"`
}

func Login(c *gin.Context) {
	user := &User{}
	_ = json.NewDecoder(c.Request.Body).Decode(user)
	if len(user.NroDni) < 8 {
		c.String(http.StatusBadRequest, "NÚMERO DE DOCUMENTO NO VALIDO")
		return
	}
	if user.Clave == "" {
		c.String(http.StatusBadRequest, "CONTRASEÑA NO VALIDA")
		return
	}
	login(c, user)
}

func login(c *gin.Context, user *User) {
	cclave := sha512.New()
	cclave.Write([]byte(user.Clave))
	cclahex := hex.EncodeToString(cclave.Sum(nil))

	// Validar documento de identidad en la DB
	lcSql := `SELECT cNroDni, cNombre, cClave, cClaAca FROM S01MPER WHERE cEstado = 'A' AND `
	if len(user.NroDni) == 8 {
		lcSql += `cNroDni = $1`
	} else {
		lcSql += `cNroDoc = $1`
	}
	err := ERPPool.QueryRow(context.Background(), lcSql, user.NroDni).Scan(&user.NroDni, &user.Nombre, &user.Clave, &user.ClaAca)
	if err == sql.ErrNoRows {
		c.String(http.StatusBadRequest, "DOCUMENTO DE IDENTIDAD NO REGISTRADO")
		return
	} else if err != nil {
		c.String(http.StatusInternalServerError, "ERROR VALIDANDO DOCUMENTO DE IDENTIDAD")
		return
	}
	// Validar si el usuario esta asignado y/o activo
	lcSql = `SELECT cEstado, cCodUsu, TRIM(cCargo), cNivel FROM V_S01TUSU_1 WHERE cNroDni = $1 AND cEstado = 'A';`
	err = ERPPool.QueryRow(context.Background(), lcSql, user.NroDni).Scan(&user.Estado, &user.CodUsu, &user.Cargo, &user.Nivel)
	if err == sql.ErrNoRows {
		c.String(http.StatusBadRequest, "USUARIO NO ASIGNADO O NO ESTÁ ACTIVO")
		return
	} else if err != nil {
		c.String(http.StatusInternalServerError, "ERROR RECUPERANDO ESTADO DE USUARIO")
		return
	}
	// Validar contraseña del usuario
	lcSql = `SELECT cCodDoc FROM A01MDOC WHERE cCodDoc = $1 AND cEstado = 'A'`
	err = ERPPool.QueryRow(context.Background(), lcSql, user.CodUsu).Scan()
	lbAuth := false
	if err != nil {
		if err != pgx.ErrNoRows {
			c.String(http.StatusInternalServerError, "ERROR VALIDANDO CONTRASEÑA")
			return
		} else if err == pgx.ErrNoRows {
			lbAuth = cclahex == user.Clave
		} else {
			lbAuth = cclahex == user.ClaAca
		}
	}
	if !lbAuth {
		c.String(http.StatusBadRequest, "CONTRASEÑA INCORRECTA")
		return
	}
	// Recuperar centro de costo asignado al usuario
	lcSql = `SELECT cCenCos FROM S01PCCO WHERE cCodUsu = $1 AND cEstado = 'A' ORDER BY cCenCos LIMIT 1`
	err = ERPPool.QueryRow(context.Background(), lcSql, user.CodUsu).Scan(&user.CenCos)
	if err == sql.ErrNoRows {
		user.CenCos = "*"
	} else if err != nil {
		c.String(http.StatusInternalServerError, "ERROR RECUPERANDO CENTROS DE COSTO")
		return
	}
	// Recuperar descripción del centro de costo
	lcSql = `SELECT cDescri FROM S01TCCO WHERE cCenCos = $1`
	err = ERPPool.QueryRow(context.Background(), lcSql, user.CenCos).Scan(&user.DesCco)
	if err != nil {
		c.String(http.StatusInternalServerError, "ERROR RECUPERANDO DESCRIPCIÓN DEL CENTRO DE COSTO")
		return
	}
	user.Clave = ""
	user.ClaAca = ""

	user.AccTkn, err = CreateAccessToken(user.CodUsu)
	if err != nil {
		c.String(http.StatusInternalServerError, "ERROR GENERANDO ACCESS TOKEN")
		return
	}

	refresh, timeout, err := CreateRefreshToken(c, user.CodUsu)
	if err != nil {
		c.String(http.StatusInternalServerError, "ERROR GENERANDO REFRESH TOKEN")
		return
	}

	c.SetCookie("UCSM", refresh, timeout, "/", "", true, true)
	c.JSON(http.StatusOK, user)
}
