package handlers

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"
)

func InitMenu(c *gin.Context) {
	code := ValidateAccessToken(c)
	if code != 0 {
		c.AbortWithStatus(code)
		return
	}
	type Request struct {
		CodUsu string `json:"CCODUSU"`
	}
	req := &Request{}
	err := json.NewDecoder(c.Request.Body).Decode(&req)
	if err != nil {
		c.String(http.StatusBadRequest, "ERROR EN DATOS DE SOLICITUD")
		return
	}
	initMenu(c, req.CodUsu)
}
func initMenu(c *gin.Context, cCodUsu string) {
	// Roles asignados a usuario
	lcSql := `SELECT DISTINCT cCodRol, cDesRol FROM V_S01TUSU_2	WHERE cCodUsu = $1 AND cEstRol = 'A' ORDER BY cCodRol`
	rows, err := ERPPool.Query(context.Background(), lcSql, cCodUsu)
	if err != nil {
		c.String(http.StatusInternalServerError, "ERROR RECUPERANDO ROLES")
		return
	}

	type role struct {
		CodRol string
		DesRol string
	}
	userRoles := []role{}

	for rows.Next() {
		rol := role{}
		if err := rows.Scan(&rol.CodRol, &rol.DesRol); err != nil {
			c.String(http.StatusInternalServerError, "ERROR LEYENDO RESPUESTA DE LA BASE DE DATOS")
			return
		}
		userRoles = append(userRoles, rol)
	}

	// Opciones asignadas a usuario
	lcSql = `SELECT cCodRol, cDesRol, cCodOpc, cDesOpc FROM V_S01TUSU_2	WHERE cCodUsu = $1 AND cEstOpc = 'A' ORDER BY cCodRol, nOrden, cCodOpc`
	rows, err = ERPPool.Query(context.Background(), lcSql, cCodUsu)
	if err != nil {
		c.String(http.StatusInternalServerError, "ERROR RECUPERANDO OPCIONES")
		return
	}

	type options struct {
		CodRol string
		DesRol string
		CodOpc string
		DesOpc string
	}
	userOpc := []options{}

	for rows.Next() {
		opc := options{}
		if err := rows.Scan(&opc.CodRol, &opc.DesRol, &opc.CodOpc, &opc.DesOpc); err != nil {
			c.String(http.StatusInternalServerError, "ERROR LEYENDO RESPUESTA DE LA BASE DE DATOS")
			return
		}
		userOpc = append(userOpc, opc)
	}

	// Periodo activo del sistema
	var period string
	lcSql = "SELECT cPeriod FROM S01TPER WHERE cTipo = 'S0'"
	err = ERPPool.QueryRow(context.Background(), lcSql).Scan(&period)
	if err != nil {
		c.String(http.StatusInternalServerError, "ERROR RECUPERANDO PERIODO ACTIVO")
		return
	}

	type response struct {
		Roles    []role
		Opciones []options
		Periodo  string
	}

	res := &response{
		Roles:    userRoles,
		Opciones: userOpc,
		Periodo:  period,
	}

	c.JSON(http.StatusOK, res)
}
