import { useState, useEffect } from "react";
import useApi from "../hooks/useApi";
import useAuth from "../hooks/useAuth";
import { useNavigate, useLocation, Link } from "react-router-dom";
import {
   Box,
   Grid,
   Container,
   Center,
   FormControl,
   FormLabel,
   Input,
   Text,
   Button,
   Image,
   Heading,
   Divider,
   Flex,
   Accordion,
   AccordionItem,
   AccordionButton,
   AccordionPanel,
   AccordionIcon,
} from "@chakra-ui/react";

const Menu = () => {
   const api = useApi();
   const { auth } = useAuth();
   let [userData, setUserData] = useState(null);
   let [user, setUser] = useState(null);
   const navigate = useNavigate();
   const location = useLocation();

   const initMenu = async () => {
      try {
         let { data } = await api.post("/initMenu", { CCODUSU: auth.codusu });
         setUserData(data);
      } catch (error) {
         console.log(error);
         navigate("/login", { state: { from: location }, replace: true });
      }
   };

   useEffect(() => {
      initMenu();
   }, []);

   return (
      <Flex flexDirection="column" h="100vh">
         <Box textAlign="center" fontSize="xl" flexGrow="1" p={5}>
            <Box maxW="container.xxl" borderRadius="md" bg="white" border="1px solid gray">
               <Box bg="secondary">
                  <Flex alignItems="center">
                     <Heading size="md" color="white" py={3} pl={3}>
                        Menú de opciones
                     </Heading>
                     <div style={{ flexGrow: 1 }}></div>
                     <Heading size="md" color="white" pr={3}>
                        {user?.CCODUSU}
                     </Heading>
                  </Flex>
               </Box>
               <Accordion allowToggle>
                  {userData?.Roles.map((item, idx) => (
                     <AccordionItem key={`userRole-${idx}`}>
                        <AccordionButton>
                           <Box flex="1" textAlign="left">
                              <Heading size="md">{item?.DesRol}</Heading>
                           </Box>
                           <AccordionIcon />
                        </AccordionButton>
                        <AccordionPanel>
                           <Flex flexDirection="column">
                              {userData?.Opciones.map((opc, itemIdx) =>
                                 opc?.CodRol === item.CodRol ? (
                                    <Button
                                       key={`menuItem-${idx}-${itemIdx}`}
                                       justifyContent="start"
                                       variant="outline"
                                       as="a"
                                       href={
                                          opc.CodRol == "ALV"
                                             ? `http://localhost:3001/${opc.CodOpc}`
                                             : "http://localhost/UCSMERP"
                                       }
                                    >
                                       {opc?.DesOpc}
                                    </Button>
                                 ) : null
                              )}
                           </Flex>
                        </AccordionPanel>
                     </AccordionItem>
                  ))}
               </Accordion>
            </Box>
         </Box>
      </Flex>
   );
};

export default Menu;
