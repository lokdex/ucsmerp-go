import { api } from "../api";
import useAuth from "../hooks/useAuth";
import { useForm } from "react-hook-form";
import { useNavigate, useLocation, useSearchParams } from "react-router-dom";

import {
   Flex,
   Container,
   FormErrorMessage,
   FormLabel,
   FormControl,
   Input,
   Button,
   Image,
   Heading,
   Divider,
   useToast,
} from "@chakra-ui/react";

import Background from "../images/background.jpg";
import Imagotipo from "../images/ucsm-imagotipo-color.svg";

const Login = () => {
   const toast = useToast();

   const {
      handleSubmit,
      register,
      formState: { errors, isSubmitting },
   } = useForm();

   const { setAuth } = useAuth();

   const navigate = useNavigate();
   const location = useLocation();
   const [params] = useSearchParams();
   const from = location.state?.from?.pathname || "/";

   const iniciarSesion = async (values) => {
      try {
         const { data } = await api.post("/login", values);
         let codusu = data?.CCODUSU;
         let accessToken = data?.Access;
         setAuth({ codusu, accessToken });
         if (params.get("from")) {
            // /${params.get("sub")}
            return window.location.replace(`http://localhost:3001${params.get("from")}`);
         }
         navigate(from, { replace: true });
      } catch (error) {
         switch (error.response.status) {
            case 400:
               toast({
                  title: "Error",
                  description: error.response.data,
                  status: "error",
                  duration: 1500,
                  isClosable: true,
               });
               break;
            case 500:
               toast({
                  title: "Error",
                  description: error.response.data,
                  status: "error",
                  duration: 1500,
                  isClosable: true,
               });
               break;
            default:
               toast({
                  title: "Error",
                  description: "Error en la solicitud",
                  status: "error",
                  duration: 1500,
                  isClosable: true,
               });
         }
      }
   };
   return (
      <Flex
         style={{
            backgroundImage: `url(${Background})`,
            backgroundRepeat: "no-repeat",
            backgroundPosition: "center",
            backgroundSize: "cover",
         }}
         direction="column"
         alignItems="center"
         height="90vh"
      >
         <Container borderRadius="md" bg="white" p={5} textAlign="center" m="auto">
            <Image src={Imagotipo} mx="auto" />
            <Heading>Sistema ERP</Heading>
            <Divider />
            <form onSubmit={handleSubmit(iniciarSesion)}>
               <FormControl isInvalid={!!errors.CNRODNI}>
                  <FormLabel htmlFor="cNroDni">Nro. de documento</FormLabel>
                  <Input
                     id="cNroDni"
                     {...register("CNRODNI", {
                        required: "Este campo es necesario",
                        minLength: { value: 8, message: "La longitud mínima debe ser 8 caracteres" },
                     })}
                  />
                  <FormErrorMessage>{errors.CNRODNI && errors.CNRODNI.message}</FormErrorMessage>
               </FormControl>
               <FormControl isInvalid={!!errors.CCLAVE}>
                  <FormLabel htmlFor="cClave">Contraseña</FormLabel>
                  <Input
                     id="cClave"
                     type="password"
                     {...register("CCLAVE", {
                        required: "Este campo es necesario",
                        minLength: { value: 6, message: "La contraseña debe tener al menos 6 caracteres" },
                     })}
                  />
                  <FormErrorMessage>{errors.CCLAVE && errors.CCLAVE.message}</FormErrorMessage>
               </FormControl>
               <Button
                  type="submit"
                  isLoading={isSubmitting}
                  bgColor="brand"
                  color="white"
                  variant="solid"
                  mt={3}
                  w="50%"
               >
                  Ingresar
               </Button>
            </form>
         </Container>
      </Flex>
   );
};

export default Login;
