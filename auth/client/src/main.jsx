import React from "react";
import { createRoot } from "react-dom/client";
import { ChakraProvider, extendTheme } from "@chakra-ui/react";
import App from "./App";
import { AuthProvider } from "./context/AuthProvider";
import { BrowserRouter, Routes, Route } from "react-router-dom";

const colors = {
   brand: "#05BE6A",
   secondary: "#fbc804",
};

const theme = extendTheme({ colors });
const container = document.getElementById("root");
const root = createRoot(container);

root.render(
   // <React.StrictMode>
   <ChakraProvider theme={theme} resetCSS>
      <BrowserRouter>
         <AuthProvider>
            <Routes>
               <Route path="/*" element={<App />} />
            </Routes>
         </AuthProvider>
      </BrowserRouter>
   </ChakraProvider>
   // </React.StrictMode>
);
